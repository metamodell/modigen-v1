/*************************************************************************************
 *
 * Spray diagram definition
 * 
 * This file contains the definition of a graphical editor using the Spray Language.
 * Refer to http://code.google.com/a/eclipselabs.org/p/spray/ for documentation.
 *
 * See also electric_circuit.properties to configure generator properties.
 *************************************************************************************/
// Add import statements here
import Electric_Plan_DSL.*

diagram Myelectric_circuit for Circuit_Plan style Electric_circuitDefaultStyle


// Add class mappings here. Refer to EClasses here. Don't forget to configure a
// dependency to the plugin defining the EMF metamodel in META-INF/MANIFEST.MF.
// It is required to have a genmodel for the metamodel.
//
// Example class mapping:
//   class BusinessClass icon "ecore/EClass.gif" {
//     shape RectangleShape
//     behavior {
//       // Enable create feature for this class and add it to the palette "Shapes"
//       create into types palette "Shapes";
//     }
//   }
//
// Example association mapping:
//   class Association icon "connection16.gif" {
//     connection ArrowConnection {
//       from source;
//       to target;
//     }
//   }

class Lamp{
	shape Lamp{
		circuit_name into LampText
	}
	behavior {
		create into circuit_referenz palette "Output Devices"
	}
}

class Horizontal_Source{
	shape HorizontalSource{
		circuit_name into HorizontalSourceText
	}
	behavior {
		create into circuit_referenz palette "Power Supplies"
	}
}

class Vertical_Source{
	shape VerticalSource{
		circuit_name into VerticalSourceText
	}
	behavior {
		create into circuit_referenz palette "Power Supplies"
	}
}

class Horizontal_Resistor{
	shape HorizontalResistor{
		circuit_name into HorizontalResistorText
	}
	behavior {
		create into circuit_referenz palette "Resistors"
	}
}

class Vertical_Resistor{
	shape VerticalResistor{
		circuit_name into VerticalResistorText
	}
	behavior {
		create into circuit_referenz palette "Resistors"
	}
}

class Horizontal_Diode{
	shape HorizontalDiode{
		circuit_name into HorizontalDiodeText
	}
	behavior {
		create into circuit_referenz palette "Diode"
	}
}

class Vertical_Diode{
	shape VerticalDiode{
		circuit_name into VerticalDiodeText
	}
	behavior {
		create into circuit_referenz palette "Diode"
	}
}

class Horizontal_On_Switch{
	shape HorizontalOnSwitch{
		circuit_name into HorizontalOnSwitchText
	}
	behavior{
		create into circuit_referenz palette "Switch"
	}
}

class Vertical_On_Switch{
	shape VerticalOnSwitch{
		circuit_name into VerticalOnSwitchText
	}
	behavior{
		create into circuit_referenz palette "Switch"
	}
}

class Horizontal_Off_Switch{
	shape HorizontalOffSwitch{
		circuit_name into HorizontalOffSwitchText
	}
	behavior{
		create into circuit_referenz palette "Switch"
	}
}

class Vertical_Off_Switch{
	shape VerticalOffSwitch{
		circuit_name into VerticalOffSwitchText
	}
	behavior{
		create into circuit_referenz palette "Switch"
	}
}
 
class Vertical_Transformator{
 	shape VerticalTransformer{
 		circuit_name into VerticalTransformerText
 	}
 	behavior{
		create into circuit_referenz palette "Power Supplies"
	}
}

class Horizontal_Transformator{
	shape HorizontalTransformer{
		circuit_name into HorizontalTransformerText
	}
	behavior{
		create into circuit_referenz palette "Power Supplies"
	}
}

class Horizontal_Transistor{
	shape HorizontalTransistor{
		circuit_name into HorizontalTransistorText
	}
	behavior{
		create into circuit_referenz palette "Transistor"
	}
}

class Vertical_Transistor{
	shape VerticalTransistor{
		circuit_name into VerticalTransistorText 
	}
	behavior{
		create into circuit_referenz palette "Transistor"
	}
}

class Voltmeter{
	shape Voltmeter{
		circuit_name into VoltmeterText
	}
	behavior{
		create into circuit_referenz palette "Meters"
	}
}

class Ammeter{
	shape Ammeter{
		circuit_name into AmmeterText
	}
	behavior{
		create into circuit_referenz palette "Meters"
	}
}

class Vertical_Fuse{
	shape VerticalFuse{
		circuit_name into VerticalFuseText
	}
	behavior{
		create into circuit_referenz palette "Power Supplies"
	}
}

class Horizontal_Fuse{
	shape HorizontalFuse{
		circuit_name into HorizontalFuseText
	}
	behavior{
		create into circuit_referenz palette "Power Supplies"
	}
}

class Vertical_Thermistor{
	shape VerticalThermistor{
		circuit_name into VerticalThermistorText
	}
	behavior{
		create into circuit_referenz palette "Sensors"
	}
}

class Horizontal_Thermistor{
	shape HorizontalThermistor{
		circuit_name into HorizontalThermistorText
	}
	behavior{
		create into circuit_referenz palette "Sensors"
	}
}

class Vertical_Variabel_Resistor{
	shape VerticalVariabelResistor{
		circuit_name into VerticalVariabelResistorText
	}
	behavior{
		create into circuit_referenz palette "Resistors"
	}
}

class Horizontal_Variabel_Resistor{
	shape HorizontalVariabelResistor{
		circuit_name into HorizontalVariabelResistorText
	}
	behavior{
		create into circuit_referenz palette "Resistors"
	}
}

class Vertical_Inductor{
	shape VerticalInductor{
		circuit_name into VerticalInductorText
	}
	behavior{
		create into circuit_referenz palette "Output Devices"
	}
}

class Horizontal_Inductor{
	shape HorizontalInductor{
		circuit_name into HorizontalInductorText
	}
	behavior{
		create into circuit_referenz palette "Output Devices"
	}
}

class Vertical_Capacitor{
	shape VerticalCapacitor{
		circuit_name into VerticalCapacitorText
	}
	behavior{
		create into circuit_referenz palette "Capacitor"
	}
}

class Horizontal_Capacitor{
	shape HorizontalCapacitor{
		circuit_name into HorizontalCapacitorText
	}
	behavior{
		create into circuit_referenz palette "Capacitor"
	}
}

class Vertical_Amplifier{
	shape VerticalAmplifier{
		circuit_name into VerticalAmplifierText
	}
	behavior{
		create into circuit_referenz palette "Audio Devices"
	}
}

class Horizontal_Amplifier{
	shape HorizontalAmplifier{
		circuit_name into HorizontalAmplifierText
	}
	behavior{
		create into circuit_referenz palette "Audio Devices"
	}
}

class Motor{
	shape Motor{
		circuit_name into MotorText
	}
	behavior{
		create into circuit_referenz palette "Output Devices"
	}
}

class Ground{
	shape Ground{
		circuit_name into GroundText
	}
	behavior{
		create into circuit_referenz palette "Power Supplies"
	}
}

class Horizontal_Loudspeaker{
	shape HorizontalLoudspeaker{
		circuit_name into HorizontalLoudspeakerText
	}
	behavior{
		create into circuit_referenz palette "Audio Devices"
	}
}

class Vertical_Loudspeaker{
	shape VerticalLoudspeaker{
		circuit_name into VerticalLoudspeaker
	}
	behavior{
		create into circuit_referenz palette "Audio Devices"
	}
}

class Cabel_Corner{
	shape JoinedCabel{
		circuit_name into JoinedCabelText
	}
	behavior{
		create into circuit_referenz palette "Wire"
	}
}

class Wire{
	connection WireConnect{
		from fromComponent
		to toComponent
	}
	behavior{
		create into circuit_referenz palette "Wire"
	}
}
 