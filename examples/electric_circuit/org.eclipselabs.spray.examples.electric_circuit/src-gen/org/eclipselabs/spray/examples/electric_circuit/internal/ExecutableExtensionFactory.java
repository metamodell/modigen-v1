/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray ExecutableExtensionFactory.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.internal;

import com.google.inject.Injector;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;
import org.eclipselabs.spray.examples.electric_circuit.Activator;

public class ExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Bundle getBundle() {
        return Activator.getDefault().getBundle();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Injector getInjector() {
        return Activator.getDefault().getInjector();
    }

}
