/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:35 CET 2015 by Spray CopyFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class Myelectric_circuitCopyFeature extends Myelectric_circuitCopyFeatureBase {
    public Myelectric_circuitCopyFeature(IFeatureProvider fp) {
        super(fp);
    }
}
