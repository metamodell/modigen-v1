/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray AddConnectionFromDslFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.services.IGaService;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractAddConnectionFeature;
import org.eclipselabs.spray.runtime.graphiti.styles.ISprayStyle;
import org.eclipselabs.spray.runtime.graphiti.rendering.ConnectionRendering;
import org.eclipselabs.spray.examples.electric_circuit.styles.Electric_circuitDefaultStyle;
import org.eclipselabs.spray.runtime.graphiti.shape.ISprayConnection;
import org.eclipselabs.spray.examples.electric_circuit.shapes.WireConnectConnection;
import com.google.common.base.Function;
import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import org.eclipselabs.spray.examples.electric_circuit.Activator;

@SuppressWarnings("unused")
public abstract class Myelectric_circuitAddWireFeatureBase extends AbstractAddConnectionFeature {

    public Myelectric_circuitAddWireFeatureBase(final IFeatureProvider fp) {
        super(fp);
        gaService = Activator.get(IGaService.class);
    }

    /**
     * {@inheritDoc}
     * 
     * @return <code>true</code> if given business object is an {@link Electric_Plan_DSL.Wire} and context is of type {@link IAddConnectionContext}
     */
    @Override
    public boolean canAdd(IAddContext context) {
        if (context instanceof IAddConnectionContext && context.getNewObject() instanceof Electric_Plan_DSL.Wire) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PictogramElement add(IAddContext context) {
        IAddConnectionContext addConContext = (IAddConnectionContext) context;
        // TODO: Domain object
        Electric_Plan_DSL.Wire addedDomainObject = (Electric_Plan_DSL.Wire) context.getNewObject();
        final ISprayStyle style = new org.eclipselabs.spray.examples.electric_circuit.styles.Electric_circuitDefaultStyle();
        ISprayConnection connection = new WireConnectConnection(getFeatureProvider());
        Connection result = (Connection) connection.getConnection(getDiagram(), style, addConContext.getSourceAnchor(), addConContext.getTargetAnchor());

        // render the connections between the same Start- and End-Anchor
        ConnectionRendering.startRendering(addConContext.getSourceAnchor(), addConContext.getTargetAnchor());

        // create link and wire it
        peService.setPropertyValue(result, PROPERTY_MODEL_TYPE, Electric_Plan_DSLPackage.Literals.WIRE.getName());
        link(result, addedDomainObject);
        for (ConnectionDecorator conDecorator : result.getConnectionDecorators()) {
            link(conDecorator, addedDomainObject);
        }

        setDoneChanges(true);
        updatePictogramElement(result);

        return result;
    }
}
