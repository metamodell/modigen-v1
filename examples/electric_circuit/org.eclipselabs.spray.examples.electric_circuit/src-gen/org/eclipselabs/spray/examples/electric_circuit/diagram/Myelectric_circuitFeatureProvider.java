/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray FeatureProvider.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.diagram;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;

public class Myelectric_circuitFeatureProvider extends Myelectric_circuitFeatureProviderBase {

    public Myelectric_circuitFeatureProvider(final IDiagramTypeProvider dtp) {
        super(dtp);
    }

}
