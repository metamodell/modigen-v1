/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:34 CET 2015 by Spray ToolBehaviorProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.diagram;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.palette.IPaletteCompartmentEntry;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.tb.AbstractSprayToolBehaviorProvider;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.services.Graphiti;

import com.google.common.collect.Lists;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateAmmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateCabel_CornerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateGroundFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateLampFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateMotorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVoltmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateWireFeature;

public abstract class Myelectric_circuitToolBehaviorProviderBase extends AbstractSprayToolBehaviorProvider {
    protected static final String COMPARTMENT_SWITCH         = "Switch";
    protected static final String COMPARTMENT_METERS         = "Meters";
    protected static final String COMPARTMENT_SENSORS        = "Sensors";
    protected static final String COMPARTMENT_DIODE          = "Diode";
    protected static final String COMPARTMENT_POWER_SUPPLIES = "Power Supplies";
    protected static final String COMPARTMENT_AUDIO_DEVICES  = "Audio Devices";
    protected static final String COMPARTMENT_CAPACITOR      = "Capacitor";
    protected static final String COMPARTMENT_WIRE           = "Wire";
    protected static final String COMPARTMENT_OUTPUT_DEVICES = "Output Devices";
    protected static final String COMPARTMENT_RESISTORS      = "Resistors";
    protected static final String COMPARTMENT_TRANSISTOR     = "Transistor";

    public Myelectric_circuitToolBehaviorProviderBase(final IDiagramTypeProvider dtp) {
        super(dtp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GraphicsAlgorithm getSelectionBorder(PictogramElement pe) {
        boolean isFromDsl = SprayLayoutService.isShapeFromDsl(pe);
        if (isFromDsl) {
            ContainerShape container = (ContainerShape) pe;
            if (!container.getChildren().isEmpty()) {
                return container.getChildren().get(0).getGraphicsAlgorithm();
            }
        }
        return super.getSelectionBorder(pe);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void buildCreationTools() {
        buildCreationTool(new Myelectric_circuitCreateLampFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_SourceFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_SourceFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_ResistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_ResistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_DiodeFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_DiodeFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_On_SwitchFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_On_SwitchFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_Off_SwitchFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_Off_SwitchFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_TransformatorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_TransformatorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_TransistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_TransistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVoltmeterFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateAmmeterFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_FuseFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_FuseFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_ThermistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_ThermistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_Variabel_ResistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_Variabel_ResistorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_InductorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_InductorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_CapacitorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_CapacitorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_AmplifierFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_AmplifierFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateMotorFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateGroundFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateHorizontal_LoudspeakerFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateVertical_LoudspeakerFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateCabel_CornerFeature(this.getFeatureProvider()));
        buildCreationTool(new Myelectric_circuitCreateWireFeature(this.getFeatureProvider()));
        // Compartments
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Iterable<IPaletteCompartmentEntry> buildPaletteCompartments() {
        return Lists.newArrayList(getPaletteCompartment(COMPARTMENT_SWITCH), getPaletteCompartment(COMPARTMENT_METERS), getPaletteCompartment(COMPARTMENT_SENSORS), getPaletteCompartment(COMPARTMENT_DIODE), getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES), getPaletteCompartment(COMPARTMENT_AUDIO_DEVICES), getPaletteCompartment(COMPARTMENT_CAPACITOR), getPaletteCompartment(COMPARTMENT_WIRE), getPaletteCompartment(COMPARTMENT_OUTPUT_DEVICES), getPaletteCompartment(COMPARTMENT_RESISTORS), getPaletteCompartment(COMPARTMENT_TRANSISTOR), getPaletteCompartment(COMPARTMENT_DEFAULT));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IPaletteCompartmentEntry getPaletteCompartmentForFeature(final IFeature feature) {
        if (feature instanceof Myelectric_circuitCreateLampFeature) {
            return getPaletteCompartment(COMPARTMENT_OUTPUT_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_SourceFeature) {
            return getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES);
        } else if (feature instanceof Myelectric_circuitCreateVertical_SourceFeature) {
            return getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_ResistorFeature) {
            return getPaletteCompartment(COMPARTMENT_RESISTORS);
        } else if (feature instanceof Myelectric_circuitCreateVertical_ResistorFeature) {
            return getPaletteCompartment(COMPARTMENT_RESISTORS);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_DiodeFeature) {
            return getPaletteCompartment(COMPARTMENT_DIODE);
        } else if (feature instanceof Myelectric_circuitCreateVertical_DiodeFeature) {
            return getPaletteCompartment(COMPARTMENT_DIODE);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_On_SwitchFeature) {
            return getPaletteCompartment(COMPARTMENT_SWITCH);
        } else if (feature instanceof Myelectric_circuitCreateVertical_On_SwitchFeature) {
            return getPaletteCompartment(COMPARTMENT_SWITCH);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_Off_SwitchFeature) {
            return getPaletteCompartment(COMPARTMENT_SWITCH);
        } else if (feature instanceof Myelectric_circuitCreateVertical_Off_SwitchFeature) {
            return getPaletteCompartment(COMPARTMENT_SWITCH);
        } else if (feature instanceof Myelectric_circuitCreateVertical_TransformatorFeature) {
            return getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_TransformatorFeature) {
            return getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_TransistorFeature) {
            return getPaletteCompartment(COMPARTMENT_TRANSISTOR);
        } else if (feature instanceof Myelectric_circuitCreateVertical_TransistorFeature) {
            return getPaletteCompartment(COMPARTMENT_TRANSISTOR);
        } else if (feature instanceof Myelectric_circuitCreateVoltmeterFeature) {
            return getPaletteCompartment(COMPARTMENT_METERS);
        } else if (feature instanceof Myelectric_circuitCreateAmmeterFeature) {
            return getPaletteCompartment(COMPARTMENT_METERS);
        } else if (feature instanceof Myelectric_circuitCreateVertical_FuseFeature) {
            return getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_FuseFeature) {
            return getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES);
        } else if (feature instanceof Myelectric_circuitCreateVertical_ThermistorFeature) {
            return getPaletteCompartment(COMPARTMENT_SENSORS);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_ThermistorFeature) {
            return getPaletteCompartment(COMPARTMENT_SENSORS);
        } else if (feature instanceof Myelectric_circuitCreateVertical_Variabel_ResistorFeature) {
            return getPaletteCompartment(COMPARTMENT_RESISTORS);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_Variabel_ResistorFeature) {
            return getPaletteCompartment(COMPARTMENT_RESISTORS);
        } else if (feature instanceof Myelectric_circuitCreateVertical_InductorFeature) {
            return getPaletteCompartment(COMPARTMENT_OUTPUT_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_InductorFeature) {
            return getPaletteCompartment(COMPARTMENT_OUTPUT_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateVertical_CapacitorFeature) {
            return getPaletteCompartment(COMPARTMENT_CAPACITOR);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_CapacitorFeature) {
            return getPaletteCompartment(COMPARTMENT_CAPACITOR);
        } else if (feature instanceof Myelectric_circuitCreateVertical_AmplifierFeature) {
            return getPaletteCompartment(COMPARTMENT_AUDIO_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_AmplifierFeature) {
            return getPaletteCompartment(COMPARTMENT_AUDIO_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateMotorFeature) {
            return getPaletteCompartment(COMPARTMENT_OUTPUT_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateGroundFeature) {
            return getPaletteCompartment(COMPARTMENT_POWER_SUPPLIES);
        } else if (feature instanceof Myelectric_circuitCreateHorizontal_LoudspeakerFeature) {
            return getPaletteCompartment(COMPARTMENT_AUDIO_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateVertical_LoudspeakerFeature) {
            return getPaletteCompartment(COMPARTMENT_AUDIO_DEVICES);
        } else if (feature instanceof Myelectric_circuitCreateCabel_CornerFeature) {
            return getPaletteCompartment(COMPARTMENT_WIRE);
        } else if (feature instanceof Myelectric_circuitCreateWireFeature) {
            return getPaletteCompartment(COMPARTMENT_WIRE);
        }
        return super.getPaletteCompartmentForFeature(feature);
    }
}
