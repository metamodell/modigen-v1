/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:33 CET 2015 by Spray CreateShapeFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipselabs.spray.runtime.graphiti.containers.SampleUtil;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractCreateFeature;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
import org.eclipse.graphiti.features.context.IAreaContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import Electric_Plan_DSL.Electric_Plan_DSLFactory;
import org.eclipselabs.spray.examples.electric_circuit.diagram.Myelectric_circuitModelService;

public abstract class Myelectric_circuitCreateCabel_CornerFeatureBase extends AbstractCreateFeature {
    protected static String                  TITLE         = "Create ";
    protected static String                  USER_QUESTION = "Enter new  name";
    protected Myelectric_circuitModelService modelService;
    protected Electric_Plan_DSL.Cabel_Corner newClass      = null;

    public Myelectric_circuitCreateCabel_CornerFeatureBase(final IFeatureProvider fp) {
        // set name and description of the creation feature
        super(fp, "Cabel_Corner", "Create new Cabel_Corner");
        modelService = Myelectric_circuitModelService.getModelService(fp.getDiagramTypeProvider());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreate(final ICreateContext context) {
        final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
        // TODO: Respect the cardinality of the containment reference
        if (context.getTargetContainer() instanceof Diagram) {
            return true;
        } else if (context.getTargetContainer() instanceof ContainerShape) {
        }
        // And now the new stuff
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] create(final ICreateContext context) {
        newClass = createCabel_Corner(context);

        if (newClass == null) {
            return EMPTY;
        }

        // do the add
        addGraphicalRepresentation(context, newClass);

        // activate direct editing after object creation
        getFeatureProvider().getDirectEditingInfo().setActive(true);

        // return newly created business object(s)
        return new Object[]{newClass};
    }

    //       org.eclipse.emf.ecore.impl.EReferenceImpl@49969416 (name: circuit_referenz) (ordered: true, unique: true, lowerBound: 0, upperBound: -1) (changeable: true, volatile: false, transient: false, defaultValueLiteral: null, unsettable: false, derived: false) (containment: true, resolveProxies: true) 

    /**
     * Creates a new {@link Electric_Plan_DSL.Cabel_Corner} instance and adds it to the containing type.
     */
    protected Electric_Plan_DSL.Cabel_Corner createCabel_Corner(final ICreateContext context) {
        // create Cabel_Corner instance
        final Electric_Plan_DSL.Cabel_Corner newClass = Electric_Plan_DSLFactory.eINSTANCE.createCabel_Corner();
        ContainerShape targetContainer = context.getTargetContainer();
        boolean isContainment = false;
        final Object target = getBusinessObjectForPictogramElement(context.getTargetContainer());
        //              And now the NEW stuff
        if (!isContainment) {
            // add the element to containment reference
            Electric_Plan_DSL.Circuit_Plan model = modelService.getModel();
            model.getCircuit_referenz().add(newClass);
        }
        setDoneChanges(true);
        return newClass;
    }
}
