/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:34 CET 2015 by Spray ImageProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.diagram;

import org.eclipse.graphiti.ui.platform.AbstractImageProvider;

public abstract class Myelectric_circuitImageProviderBase extends AbstractImageProvider {
    // The prefix for all identifiers of this image provider
    public static final String PREFIX = "org.eclipselabs.spray.examples.electric_circuit.diagram.";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void addAvailableImages() {
        // register the path for each image identifier
    }
}
