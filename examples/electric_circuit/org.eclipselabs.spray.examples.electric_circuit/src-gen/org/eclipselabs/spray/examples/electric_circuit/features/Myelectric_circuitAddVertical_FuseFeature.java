/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray AddShapeFromDslFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class Myelectric_circuitAddVertical_FuseFeature extends Myelectric_circuitAddVertical_FuseFeatureBase {
    public Myelectric_circuitAddVertical_FuseFeature(final IFeatureProvider fp) {
        super(fp);
    }
}
