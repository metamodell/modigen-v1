/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:17 CET 2015 by Spray ShapeDefinitionGenerator.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.shapes;

import org.eclipse.graphiti.features.IFeatureProvider;

public class VerticalVariabelResistorShape extends VerticalVariabelResistorShapeBase {

    public VerticalVariabelResistorShape(IFeatureProvider fp) {
        super(fp);
    }
}
