/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray DiagramTypeProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.diagram;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

public abstract class Myelectric_circuitDiagramTypeProviderBase extends AbstractDiagramTypeProvider {
    protected IToolBehaviorProvider[] toolBehaviorProviders;

    public Myelectric_circuitDiagramTypeProviderBase() {
        super();
        setFeatureProvider(new Myelectric_circuitFeatureProvider(this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
        if (toolBehaviorProviders == null) {
            toolBehaviorProviders = new IToolBehaviorProvider[]{new Myelectric_circuitToolBehaviorProvider(this)};
        }
        return toolBehaviorProviders;
    }

}
