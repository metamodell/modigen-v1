/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray AddShapeFromDslFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class Myelectric_circuitAddHorizontal_ResistorFeature extends Myelectric_circuitAddHorizontal_ResistorFeatureBase {
    public Myelectric_circuitAddHorizontal_ResistorFeature(final IFeatureProvider fp) {
        super(fp);
    }
}
