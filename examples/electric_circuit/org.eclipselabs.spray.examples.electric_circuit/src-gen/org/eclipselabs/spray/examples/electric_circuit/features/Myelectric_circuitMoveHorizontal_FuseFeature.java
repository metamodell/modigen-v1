/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:36 CET 2015 by Spray MoveFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class Myelectric_circuitMoveHorizontal_FuseFeature extends Myelectric_circuitMoveHorizontal_FuseFeatureBase {
    public Myelectric_circuitMoveHorizontal_FuseFeature(final IFeatureProvider fp) {
        super(fp);
    }
}
