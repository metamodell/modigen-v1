/**
 * This is a generated Shape for Spray
 */
package org.eclipselabs.spray.examples.electric_circuit.shapes;

import java.util.List;
import java.util.ArrayList;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;

import org.eclipse.graphiti.datatypes.IDimension;
import org.eclipse.graphiti.features.*;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;

import org.eclipse.graphiti.mm.pictograms.*;
import org.eclipse.graphiti.mm.algorithms.*;
import org.eclipse.graphiti.mm.algorithms.styles.*;

import org.eclipselabs.spray.runtime.graphiti.ISprayConstants;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayAbstractLayoutManager;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutType;

import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
import org.eclipselabs.spray.runtime.graphiti.shape.DefaultSprayShape;
import org.eclipselabs.spray.runtime.graphiti.shape.SprayLayoutManager;
import org.eclipselabs.spray.runtime.graphiti.styles.ISprayStyle;

@SuppressWarnings("all")
public class HorizontalTransistorShapeBase extends DefaultSprayShape {

    public static enum TextIds {
        HorizontalTransistorText
    }

    public HorizontalTransistorShapeBase(IFeatureProvider fp) {
        super(fp);
    }

    @Override
    public ContainerShape getShape(ContainerShape targetContainer, ISprayStyle sprayStyle) {
        // Create a ContainerShape for this Shape
        Diagram diagram = peService.getDiagramForShape(targetContainer);
        ContainerShape containerShape = peCreateService.createContainerShape(targetContainer, true);
        SprayLayoutService.setId(containerShape, "HorizontalTransistor.containerShape");

        // define general layout for ContainerShape
        sprayStyle.getStyle(diagram).setProportional(false);
        sprayStyle.getStyle(diagram).setStretchH(false);
        sprayStyle.getStyle(diagram).setStretchV(false);

        // layout data
        SprayLayoutType containerLayout = SprayLayoutType.FIT;
        SprayLayoutService.setLayoutManager(containerShape, containerLayout, 0, 0, true);
        SprayLayoutService.getLayoutData(containerShape).setVisible(true);

        createCascadedElements(diagram, containerShape, sprayStyle);
        createAnchorPoints(diagram, containerShape);

        // Fix the broken coordinate syaten for not active container shapes
        SprayAbstractLayoutManager.fixOffset(containerShape);

        return containerShape;
    }

    // START GENERATING CASCADED ELEMENTS
    protected void createCascadedElements(Diagram diagram, ContainerShape containerShape, ISprayStyle sprayStyle) {
        IDirectEditingInfo directEditingInfo = getFeatureProvider().getDirectEditingInfo();
        directEditingInfo.setMainPictogramElement(containerShape);
        directEditingInfo.setPictogramElement(containerShape);

        GraphicsAlgorithm element_0 = gaService.createInvisibleRectangle(containerShape);
        element_0.setStyle(sprayStyle.getStyle(diagram));
        SprayLayoutService.setShapeFromDsl(containerShape, true);
        gaService.setLocationAndSize(element_0, 0, 0, 80, 73);

        // Invisible rectangle around the elements (because more then one element is on first layer).
        ContainerShape invisibleShape = peCreateService.createContainerShape(containerShape, false);
        SprayLayoutService.setId(invisibleShape, "HorizontalTransistor.invisibleShape");
        SprayLayoutType layout_1 = SprayLayoutType.TOP;
        SprayLayoutService.setLayoutManager(containerShape, layout_1, 0, 0);

        GraphicsAlgorithm element_1 = gaService.createPlainRectangle(invisibleShape);
        element_1.setStyle(sprayStyle.getStyle(diagram));
        element_1.setFilled(false);
        element_1.setLineVisible(false);
        gaService.setLocationAndSize(element_1, 0, 0, 80, 73);

        createElement_2(diagram, containerShape, sprayStyle);
        createElement_3(diagram, containerShape, sprayStyle);
        createElement_4(diagram, containerShape, sprayStyle);
        createElement_5(diagram, containerShape, sprayStyle);
        createElement_6(diagram, containerShape, sprayStyle);
        createElement_7(diagram, containerShape, sprayStyle);

        // Set start values for height and width as properties on the element for Layout Feature
        SprayLayoutManager.setSizePictogramProperties(containerShape);
    }

    protected Shape createElement_2(Diagram diagram, ContainerShape parentShape, ISprayStyle sprayStyle) {
        Shape shape_2 = peCreateService.createShape(parentShape, false);
        SprayLayoutService.setId(shape_2, "HorizontalTransistor.shape_2");
        Text element_2 = gaService.createPlainText(shape_2);
        ISprayStyle style_2 = sprayStyle;
        element_2.setStyle(style_2.getStyle(diagram));
        gaService.setLocationAndSize(element_2, 0, 43, 30, 30);
        SprayLayoutService.setLayoutData(shape_2, 30, 30, true);
        element_2.setHorizontalAlignment(Orientation.ALIGNMENT_LEFT);
        element_2.setVerticalAlignment(Orientation.ALIGNMENT_TOP);
        peService.setPropertyValue(element_2, ISprayConstants.TEXT_ID, TextIds.HorizontalTransistorText.name());
        peService.setPropertyValue(shape_2, ISprayConstants.TEXT_ID, TextIds.HorizontalTransistorText.name());
        element_2.setValue("");
        getFeatureProvider().getDirectEditingInfo().setGraphicsAlgorithm(element_2);
        return shape_2;
    }

    protected Shape createElement_3(Diagram diagram, ContainerShape parentShape, ISprayStyle sprayStyle) {
        ContainerShape shape_3 = peCreateService.createContainerShape(parentShape, false);
        SprayLayoutService.setId(shape_3, "HorizontalTransistor.shape_3");
        SprayLayoutService.setCompartment(shape_3, false);

        Ellipse element_3 = gaService.createPlainEllipse(shape_3);
        ISprayStyle style_3 = sprayStyle;
        element_3.setStyle(style_3.getStyle(diagram));
        gaService.setLocationAndSize(element_3, 40, 0, 40, 40);
        SprayLayoutService.setLayoutData(shape_3, 40, 40, true);
        return shape_3;
    }

    protected Shape createElement_4(Diagram diagram, ContainerShape parentShape, ISprayStyle sprayStyle) {
        List<Point> pointList_4 = new ArrayList<Point>();
        pointList_4.add(gaService.createPoint(47, 4, 0, 0));
        pointList_4.add(gaService.createPoint(47, 35, 0, 0));
        Shape shape_4 = peCreateService.createShape(parentShape, false);
        SprayLayoutService.setId(shape_4, "HorizontalTransistor.shape_4");
        Polyline element_4 = gaService.createPlainPolyline(shape_4, pointList_4);
        ISprayStyle style_4 = sprayStyle;
        element_4.setStyle(style_4.getStyle(diagram));
        return shape_4;
    }

    protected Shape createElement_5(Diagram diagram, ContainerShape parentShape, ISprayStyle sprayStyle) {
        List<Point> pointList_5 = new ArrayList<Point>();
        pointList_5.add(gaService.createPoint(40, 20, 0, 0));
        pointList_5.add(gaService.createPoint(54, 20, 0, 0));
        Shape shape_5 = peCreateService.createShape(parentShape, false);
        SprayLayoutService.setId(shape_5, "HorizontalTransistor.shape_5");
        Polyline element_5 = gaService.createPlainPolyline(shape_5, pointList_5);
        ISprayStyle style_5 = sprayStyle;
        element_5.setStyle(style_5.getStyle(diagram));
        return shape_5;
    }

    protected Shape createElement_6(Diagram diagram, ContainerShape parentShape, ISprayStyle sprayStyle) {
        List<Point> pointList_6 = new ArrayList<Point>();
        pointList_6.add(gaService.createPoint(72, 3, 0, 0));
        pointList_6.add(gaService.createPoint(54, 20, 0, 0));
        pointList_6.add(gaService.createPoint(72, 37, 0, 0));
        ContainerShape shape_6 = peCreateService.createContainerShape(parentShape, false);
        SprayLayoutService.setId(shape_6, "HorizontalTransistor.shape_6");
        Polyline element_6 = gaService.createPlainPolyline(shape_6, pointList_6);
        ISprayStyle style_6 = sprayStyle;
        element_6.setStyle(style_6.getStyle(diagram));
        return shape_6;
    }

    protected Shape createElement_7(Diagram diagram, ContainerShape parentShape, ISprayStyle sprayStyle) {
        List<Point> pointList_7 = new ArrayList<Point>();
        pointList_7.add(gaService.createPoint(60, 33, 0, 0));
        pointList_7.add(gaService.createPoint(68, 34, 0, 0));
        pointList_7.add(gaService.createPoint(68, 26, 0, 0));
        ContainerShape shape_7 = peCreateService.createContainerShape(parentShape, false);
        SprayLayoutService.setId(shape_7, "HorizontalTransistor.shape_7");
        Polyline element_7 = gaService.createPlainPolyline(shape_7, pointList_7);
        ISprayStyle style_7 = sprayStyle;
        element_7.setStyle(style_7.getStyle(diagram));
        return shape_7;
    }

    // STOP GENERATING CASCADED ELEMENTS

    protected void createAnchorPoints(Diagram diagram, ContainerShape containerShape) {
        {
            FixPointAnchor fixAnchor = peCreateService.createFixPointAnchor(containerShape);
            Point fixAnchorPoint = gaService.createPoint(42, 20);
            fixAnchor.setLocation(fixAnchorPoint);
            Ellipse ellipse = gaService.createEllipse(fixAnchor);
            ellipse.setFilled(true);
            ellipse.setLineVisible(false);
            ellipse.setBackground(gaService.manageColor(diagram, IColorConstant.GRAY));
            ellipse.setX(-3);
            ellipse.setY(-3);
            ellipse.setWidth(6);
            ellipse.setHeight(6);
        }
        {
            FixPointAnchor fixAnchor = peCreateService.createFixPointAnchor(containerShape);
            Point fixAnchorPoint = gaService.createPoint(69, 5);
            fixAnchor.setLocation(fixAnchorPoint);
            Ellipse ellipse = gaService.createEllipse(fixAnchor);
            ellipse.setFilled(true);
            ellipse.setLineVisible(false);
            ellipse.setBackground(gaService.manageColor(diagram, IColorConstant.GRAY));
            ellipse.setX(-3);
            ellipse.setY(-3);
            ellipse.setWidth(6);
            ellipse.setHeight(6);
        }
        {
            FixPointAnchor fixAnchor = peCreateService.createFixPointAnchor(containerShape);
            Point fixAnchorPoint = gaService.createPoint(69, 35);
            fixAnchor.setLocation(fixAnchorPoint);
            Ellipse ellipse = gaService.createEllipse(fixAnchor);
            ellipse.setFilled(true);
            ellipse.setLineVisible(false);
            ellipse.setBackground(gaService.manageColor(diagram, IColorConstant.GRAY));
            ellipse.setX(-3);
            ellipse.setY(-3);
            ellipse.setWidth(6);
            ellipse.setHeight(6);
        }
    }

    public SprayLayoutManager getShapeLayout() {
        SprayLayoutManager layoutManager = new SprayLayoutManager();
        layoutManager.setMinSizeWidth(-1);
        layoutManager.setMaxSizeWidth(-1);
        layoutManager.setMinSizeHeight(-1);
        layoutManager.setMaxSizeHeight(-1);
        layoutManager.setStretchHorizontal(true);
        layoutManager.setStretchVertical(true);
        return layoutManager;
    }

}
