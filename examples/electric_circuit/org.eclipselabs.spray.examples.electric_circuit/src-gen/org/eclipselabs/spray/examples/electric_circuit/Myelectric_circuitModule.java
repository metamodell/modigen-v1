/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray GuiceModule.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit;

public class Myelectric_circuitModule extends Myelectric_circuitModuleBase {
    // Add custom bindings here
    // public Class<? extends MyInterface> bindMyInterface () {
    //   return MyInterfaceImpl.class;
    // }
    //
    // Get instances through the Activator:
    // MyInterface instance = Activator.get(MyInterface.class);
}
