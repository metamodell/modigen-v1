/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:33 CET 2015 by Spray LayoutFromDslFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class Myelectric_circuitLayoutHorizontal_ThermistorFeature extends Myelectric_circuitLayoutHorizontal_ThermistorFeatureBase {
    public Myelectric_circuitLayoutHorizontal_ThermistorFeature(final IFeatureProvider fp) {
        super(fp);
    }

}
