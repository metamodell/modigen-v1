/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray CreateConnectionFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.IGaService;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractCreateConnectionFeature;
import org.eclipselabs.spray.runtime.graphiti.containers.SampleUtil;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import Electric_Plan_DSL.Electric_Plan_DSLFactory;
import org.eclipselabs.spray.examples.electric_circuit.Activator;

public abstract class Myelectric_circuitCreateWireFeatureBase extends AbstractCreateConnectionFeature {
    protected static String TITLE         = "Create Wire";
    protected static String USER_QUESTION = "Enter new Wire name";

    public Myelectric_circuitCreateWireFeatureBase(final IFeatureProvider fp) {
        // provide name and description for the UI, e.g. the palette
        super(fp, "Wire", "Create Wire");
        gaService = Activator.get(IGaService.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreate(final ICreateConnectionContext context) {
        Anchor targetAnchor = getDslShapeAnchor(context.getTargetPictogramElement());
        if (targetAnchor == null) {
            return false;
        }
        // return true if both anchors belong to an EClass of the right type and those EClasses are not identical
        Anchor sourceAnchor = getDslShapeAnchor(context.getSourcePictogramElement());
        Electric_Plan_DSL.Component source = getComponent(sourceAnchor);
        Electric_Plan_DSL.Component target = getComponent(targetAnchor);
        if ((source != null) && (target != null) && (source != target)) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canStartConnection(final ICreateConnectionContext context) {
        // return true if start anchor belongs to a EClass of the right type
        Anchor sourceAnchor = getDslShapeAnchor(context.getSourcePictogramElement());
        if (getComponent(sourceAnchor) != null) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connection create(final ICreateConnectionContext context) {
        Connection newConnection = null;
        Anchor sourceAnchor = getDslShapeAnchor(context.getSourcePictogramElement());
        Anchor targetAnchor = getDslShapeAnchor(context.getTargetPictogramElement());

        // get EClasses which should be connected
        final Electric_Plan_DSL.Component source = getComponent(sourceAnchor);
        final Electric_Plan_DSL.Component target = getComponent(targetAnchor);
        // containment reference is not a feature of source
        final Electric_Plan_DSL.Circuit_Plan container = org.eclipse.xtext.EcoreUtil2.getContainerOfType(source, Electric_Plan_DSL.Circuit_Plan.class);
        if (source != null && target != null) {
            // create new business object
            final Electric_Plan_DSL.Wire eReference = createWire(source, target);
            // add the element to containment reference
            container.getCircuit_referenz().add(eReference);
            // add connection for business object
            final AddConnectionContext addContext = new AddConnectionContext(sourceAnchor, targetAnchor);
            addContext.setNewObject(eReference);
            newConnection = (Connection) getFeatureProvider().addIfPossible(addContext);

            // activate direct editing after object creation
            getFeatureProvider().getDirectEditingInfo().setActive(true);
        }

        return newConnection;
    }

    /**
     * Returns the Component belonging to the anchor, or <code>null</code> if not available.
     */
    protected Electric_Plan_DSL.Component getComponent(final Anchor anchor) {
        if (anchor != null) {
            final EObject bo = (EObject) getBusinessObjectForPictogramElement(anchor.getParent());
            if (bo instanceof Electric_Plan_DSL.Component) {
                return (Electric_Plan_DSL.Component) bo;
            }
        }
        return null;
    }

    /**
     * Creates a EReference between two EClasses.
     */
    protected Electric_Plan_DSL.Wire createWire(final Electric_Plan_DSL.Component source, final Electric_Plan_DSL.Component target) {
        // TODO: Domain Object
        final Electric_Plan_DSL.Wire domainObject = Electric_Plan_DSLFactory.eINSTANCE.createWire();
        domainObject.setFromComponent(source);
        domainObject.setToComponent(target);

        setDoneChanges(true);
        return domainObject;
    }

    protected Anchor getDslShapeAnchor(PictogramElement pe) {
        if (pe == null) {
            return null;
        }
        Shape dslShape = SprayLayoutService.findDslShape(pe);
        if (dslShape != null) {
            EList<Anchor> anchors = dslShape.getAnchors();
            if (!anchors.isEmpty()) {
                return anchors.get(0);
            }
        }
        return null;
    }
}
