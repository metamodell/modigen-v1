/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:34 CET 2015 by Spray ToolBehaviorProvider.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.diagram;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;

public class Myelectric_circuitToolBehaviorProvider extends Myelectric_circuitToolBehaviorProviderBase {
    public Myelectric_circuitToolBehaviorProvider(final IDiagramTypeProvider dtp) {
        super(dtp);
    }
}
