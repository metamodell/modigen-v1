/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:36 CET 2015 by Spray MoveFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class Myelectric_circuitMoveCabel_CornerFeature extends Myelectric_circuitMoveCabel_CornerFeatureBase {
    public Myelectric_circuitMoveCabel_CornerFeature(final IFeatureProvider fp) {
        super(fp);
    }
}
