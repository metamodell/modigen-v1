/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:34 CET 2015 by Spray UpdateShapeFromDslFeature.xtend
 * 
 * This file is an extension point: copy to "src" folder to manually add code to this
 * extension point.
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;

public class Myelectric_circuitUpdateHorizontal_InductorFeature extends Myelectric_circuitUpdateHorizontal_InductorFeatureBase {
    public Myelectric_circuitUpdateHorizontal_InductorFeature(final IFeatureProvider fp) {
        super(fp);
    }

}
