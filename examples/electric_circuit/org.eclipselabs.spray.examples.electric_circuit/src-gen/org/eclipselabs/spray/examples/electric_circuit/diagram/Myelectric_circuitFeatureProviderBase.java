/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:32 CET 2015 by Spray FeatureProvider.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.diagram;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.ICopyFeature;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.IMoveShapeFeature;
import org.eclipse.graphiti.features.IPasteFeature;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.IDeleteFeature;
import org.eclipse.graphiti.features.IDirectEditingFeature;
import org.eclipse.graphiti.features.IRemoveFeature;
import org.eclipse.graphiti.features.IResizeShapeFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.ICopyContext;
import org.eclipse.graphiti.features.context.IDeleteContext;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.IPasteContext;
import org.eclipse.graphiti.features.context.IRemoveContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.custom.ICustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import org.eclipselabs.spray.runtime.graphiti.GraphitiProperties;
import org.eclipselabs.spray.runtime.graphiti.features.DefaultDeleteFeature;
import org.eclipselabs.spray.runtime.graphiti.features.DefaultFeatureProvider;
import org.eclipselabs.spray.runtime.graphiti.features.DefaultRemoveFeature;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayFixedLayoutManager;
import org.eclipselabs.spray.runtime.graphiti.containers.OwnerPropertyDeleteFeature;
import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddAmmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddCabel_CornerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddGroundFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddHorizontal_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddLampFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddMotorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVertical_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddVoltmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitAddWireFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCopyFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateAmmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateCabel_CornerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateGroundFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateHorizontal_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateLampFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateMotorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVertical_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateVoltmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitCreateWireFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditAmmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditCabel_CornerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditGroundFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditHorizontal_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditLampFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditMotorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVertical_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditVoltmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitDirectEditWireFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutAmmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutCabel_CornerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutGroundFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutHorizontal_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutLampFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutMotorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVertical_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitLayoutVoltmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitPasteFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeAmmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeCabel_CornerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeGroundFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeHorizontal_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeLampFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeMotorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVertical_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitResizeVoltmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateAmmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateCabel_CornerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateGroundFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateHorizontal_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateLampFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateMotorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_AmplifierFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_CapacitorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_DiodeFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_FuseFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_InductorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_LoudspeakerFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_Off_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_On_SwitchFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_SourceFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_ThermistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_TransformatorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_TransistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVertical_Variabel_ResistorFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateVoltmeterFeature;
import org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitUpdateWireFeature;

@SuppressWarnings("unused")
public abstract class Myelectric_circuitFeatureProviderBase extends DefaultFeatureProvider {
    public Myelectric_circuitFeatureProviderBase(final IDiagramTypeProvider dtp) {
        super(dtp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IAddFeature getAddFeature(final IAddContext context) {
        // is object for add request a EClass or EReference?
        final EObject bo = (EObject) context.getNewObject();
        final String reference = (String) context.getProperty(PROPERTY_REFERENCE);
        final String alias = (String) context.getProperty(PROPERTY_ALIAS);
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.LAMP && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddLampFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_SOURCE && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_SourceFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_SOURCE && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_SourceFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_RESISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_ResistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_RESISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_ResistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_DIODE && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_DiodeFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_DIODE && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_DiodeFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_ON_SWITCH && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_On_SwitchFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_On_SwitchFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_OFF_SWITCH && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_Off_SwitchFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_Off_SwitchFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSFORMATOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_TransformatorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_TransformatorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_TransistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_TransistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VOLTMETER && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVoltmeterFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.AMMETER && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddAmmeterFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_FUSE && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_FuseFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_FUSE && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_FuseFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_THERMISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_ThermistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_THERMISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_ThermistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_VARIABEL_RESISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_Variabel_ResistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_VARIABEL_RESISTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_Variabel_ResistorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_INDUCTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_InductorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_INDUCTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_InductorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_CAPACITOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_CapacitorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_CAPACITOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_CapacitorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_AMPLIFIER && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_AmplifierFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_AMPLIFIER && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_AmplifierFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.MOTOR && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddMotorFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.GROUND && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddGroundFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_LOUDSPEAKER && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddHorizontal_LoudspeakerFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddVertical_LoudspeakerFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.CABEL_CORNER && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddCabel_CornerFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.WIRE && alias == null) {
            if (reference == null) {
                return new Myelectric_circuitAddWireFeature(this);
            }
        }
        return super.getAddFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICopyFeature getCopyFeature(ICopyContext context) {
        return new Myelectric_circuitCopyFeature(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICreateFeature[] getCreateFeatures() {
        return new ICreateFeature[]{
                new Myelectric_circuitCreateLampFeature(this),
                new Myelectric_circuitCreateHorizontal_SourceFeature(this),
                new Myelectric_circuitCreateVertical_SourceFeature(this),
                new Myelectric_circuitCreateHorizontal_ResistorFeature(this),
                new Myelectric_circuitCreateVertical_ResistorFeature(this),
                new Myelectric_circuitCreateHorizontal_DiodeFeature(this),
                new Myelectric_circuitCreateVertical_DiodeFeature(this),
                new Myelectric_circuitCreateHorizontal_On_SwitchFeature(this),
                new Myelectric_circuitCreateVertical_On_SwitchFeature(this),
                new Myelectric_circuitCreateHorizontal_Off_SwitchFeature(this),
                new Myelectric_circuitCreateVertical_Off_SwitchFeature(this),
                new Myelectric_circuitCreateVertical_TransformatorFeature(this),
                new Myelectric_circuitCreateHorizontal_TransformatorFeature(this),
                new Myelectric_circuitCreateHorizontal_TransistorFeature(this),
                new Myelectric_circuitCreateVertical_TransistorFeature(this),
                new Myelectric_circuitCreateVoltmeterFeature(this),
                new Myelectric_circuitCreateAmmeterFeature(this),
                new Myelectric_circuitCreateVertical_FuseFeature(this),
                new Myelectric_circuitCreateHorizontal_FuseFeature(this),
                new Myelectric_circuitCreateVertical_ThermistorFeature(this),
                new Myelectric_circuitCreateHorizontal_ThermistorFeature(this),
                new Myelectric_circuitCreateVertical_Variabel_ResistorFeature(this),
                new Myelectric_circuitCreateHorizontal_Variabel_ResistorFeature(this),
                new Myelectric_circuitCreateVertical_InductorFeature(this),
                new Myelectric_circuitCreateHorizontal_InductorFeature(this),
                new Myelectric_circuitCreateVertical_CapacitorFeature(this),
                new Myelectric_circuitCreateHorizontal_CapacitorFeature(this),
                new Myelectric_circuitCreateVertical_AmplifierFeature(this),
                new Myelectric_circuitCreateHorizontal_AmplifierFeature(this),
                new Myelectric_circuitCreateMotorFeature(this),
                new Myelectric_circuitCreateGroundFeature(this),
                new Myelectric_circuitCreateHorizontal_LoudspeakerFeature(this),
                new Myelectric_circuitCreateVertical_LoudspeakerFeature(this),
                new Myelectric_circuitCreateCabel_CornerFeature(this)};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICreateConnectionFeature[] getCreateConnectionFeatures() {
        return new ICreateConnectionFeature[]{new Myelectric_circuitCreateWireFeature(this)};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IUpdateFeature getUpdateFeature(final IUpdateContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        String alias;
        if (pictogramElement instanceof Shape) {
            Shape dslShape = SprayLayoutService.findDslShape(pictogramElement);
            alias = peService.getPropertyValue(dslShape, PROPERTY_ALIAS);
        } else {
            alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        }
        //    if (pictogramElement instanceof ContainerShape) {
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.LAMP && alias == null) { // 11
            return new Myelectric_circuitUpdateLampFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_SOURCE && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_SOURCE && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_RESISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_RESISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_DIODE && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_DIODE && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_ON_SWITCH && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_OFF_SWITCH && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSFORMATOR && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VOLTMETER && alias == null) { // 11
            return new Myelectric_circuitUpdateVoltmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.AMMETER && alias == null) { // 11
            return new Myelectric_circuitUpdateAmmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_FUSE && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_FUSE && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_THERMISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_THERMISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_VARIABEL_RESISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_VARIABEL_RESISTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_INDUCTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_INDUCTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_CAPACITOR && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_CAPACITOR && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_AMPLIFIER && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_AMPLIFIER && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.MOTOR && alias == null) { // 11
            return new Myelectric_circuitUpdateMotorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.GROUND && alias == null) { // 11
            return new Myelectric_circuitUpdateGroundFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_LOUDSPEAKER && alias == null) { // 11
            return new Myelectric_circuitUpdateHorizontal_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER && alias == null) { // 11
            return new Myelectric_circuitUpdateVertical_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.CABEL_CORNER && alias == null) { // 11
            return new Myelectric_circuitUpdateCabel_CornerFeature(this);
        }
        if (bo instanceof Electric_Plan_DSL.Wire && alias == null) { // 33
            return new Myelectric_circuitUpdateWireFeature(this);
        }
        //        }
        return super.getUpdateFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ILayoutFeature getLayoutFeature(final ILayoutContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        final String alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.LAMP && alias == null) {
            return new Myelectric_circuitLayoutLampFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_SOURCE && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_SOURCE && alias == null) {
            return new Myelectric_circuitLayoutVertical_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_RESISTOR && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_RESISTOR && alias == null) {
            return new Myelectric_circuitLayoutVertical_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_DIODE && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_DIODE && alias == null) {
            return new Myelectric_circuitLayoutVertical_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_ON_SWITCH && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH && alias == null) {
            return new Myelectric_circuitLayoutVertical_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_OFF_SWITCH && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH && alias == null) {
            return new Myelectric_circuitLayoutVertical_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSFORMATOR && alias == null) {
            return new Myelectric_circuitLayoutVertical_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSISTOR && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR && alias == null) {
            return new Myelectric_circuitLayoutVertical_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VOLTMETER && alias == null) {
            return new Myelectric_circuitLayoutVoltmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.AMMETER && alias == null) {
            return new Myelectric_circuitLayoutAmmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_FUSE && alias == null) {
            return new Myelectric_circuitLayoutVertical_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_FUSE && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_THERMISTOR && alias == null) {
            return new Myelectric_circuitLayoutVertical_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_THERMISTOR && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_VARIABEL_RESISTOR && alias == null) {
            return new Myelectric_circuitLayoutVertical_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_VARIABEL_RESISTOR && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_INDUCTOR && alias == null) {
            return new Myelectric_circuitLayoutVertical_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_INDUCTOR && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_CAPACITOR && alias == null) {
            return new Myelectric_circuitLayoutVertical_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_CAPACITOR && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_AMPLIFIER && alias == null) {
            return new Myelectric_circuitLayoutVertical_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_AMPLIFIER && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.MOTOR && alias == null) {
            return new Myelectric_circuitLayoutMotorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.GROUND && alias == null) {
            return new Myelectric_circuitLayoutGroundFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_LOUDSPEAKER && alias == null) {
            return new Myelectric_circuitLayoutHorizontal_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER && alias == null) {
            return new Myelectric_circuitLayoutVertical_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.CABEL_CORNER && alias == null) {
            return new Myelectric_circuitLayoutCabel_CornerFeature(this);
        }
        return super.getLayoutFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IRemoveFeature getRemoveFeature(final IRemoveContext context) {
        // Spray specific DefaultRemoveFeature
        final PictogramElement pictogramElement = context.getPictogramElement();
        return new DefaultRemoveFeature(this);
    }

    public IDeleteFeature getDeleteFeature(final IDeleteContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        final String reference = peService.getPropertyValue(pictogramElement, PROPERTY_REFERENCE);
        final String alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);

        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.LAMP && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_SOURCE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_SOURCE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_RESISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_RESISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_DIODE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_DIODE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_ON_SWITCH && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_OFF_SWITCH && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSFORMATOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VOLTMETER && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.AMMETER && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_FUSE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_FUSE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_THERMISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_THERMISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_VARIABEL_RESISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_VARIABEL_RESISTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_INDUCTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_INDUCTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_CAPACITOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_CAPACITOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_AMPLIFIER && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_AMPLIFIER && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.MOTOR && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.GROUND && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_LOUDSPEAKER && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.CABEL_CORNER && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.WIRE && alias == null) {
            if (reference == null) {
                return new DefaultDeleteFeature(this);
            }
        }

        return new DefaultDeleteFeature(this);
    }

    /** 
     * Ensure that any shape with property {@link ISprayConstants#CAN_MOVE} set to false will not have a move feature.
     */
    /**
     * {@inheritDoc}
     */
    @Override
    public IMoveShapeFeature getMoveShapeFeature(final IMoveShapeContext context) {
        final Shape shape = context.getShape();
        final ContainerShape parent = shape.getContainer();
        EObject eObject = getBusinessObjectForPictogramElement(shape);
        ContainerShape targetContainer = context.getTargetContainer();
        EObject target = getBusinessObjectForPictogramElement(targetContainer);
        if (eObject instanceof Electric_Plan_DSL.Lamp) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveLampFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Source) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_SourceFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Source) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_SourceFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Resistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_ResistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Resistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_ResistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Diode) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_DiodeFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Diode) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_DiodeFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_On_Switch) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_On_SwitchFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_On_Switch) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_On_SwitchFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Off_Switch) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_Off_SwitchFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Off_Switch) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_Off_SwitchFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Transformator) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_TransformatorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Transformator) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_TransformatorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Transistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_TransistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Transistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_TransistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Voltmeter) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVoltmeterFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Ammeter) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveAmmeterFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Fuse) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_FuseFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Fuse) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_FuseFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Thermistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_ThermistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Thermistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_ThermistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Variabel_Resistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_Variabel_ResistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Variabel_Resistor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_Variabel_ResistorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Inductor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_InductorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Inductor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_InductorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Capacitor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_CapacitorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Capacitor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_CapacitorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Amplifier) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_AmplifierFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Amplifier) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_AmplifierFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Motor) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveMotorFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Ground) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveGroundFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Horizontal_Loudspeaker) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveHorizontal_LoudspeakerFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Vertical_Loudspeaker) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveVertical_LoudspeakerFeature(this);
        }

        if (eObject instanceof Electric_Plan_DSL.Cabel_Corner) {
            return new org.eclipselabs.spray.examples.electric_circuit.features.Myelectric_circuitMoveCabel_CornerFeature(this);
        }

        return super.getMoveShapeFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IPasteFeature getPasteFeature(IPasteContext context) {
        return new Myelectric_circuitPasteFeature(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IDirectEditingFeature getDirectEditingFeature(IDirectEditingContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        if (SprayLayoutService.isCompartment(pictogramElement)) {
            return null;
        }
        String alias = null;
        if (pictogramElement instanceof Shape) {
            Shape dslShape = SprayLayoutService.findDslShape(pictogramElement);
            alias = peService.getPropertyValue(dslShape, PROPERTY_ALIAS);
        } else {
            alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.LAMP && alias == null) {
            return new Myelectric_circuitDirectEditLampFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_SOURCE && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_SOURCE && alias == null) {
            return new Myelectric_circuitDirectEditVertical_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_RESISTOR && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_RESISTOR && alias == null) {
            return new Myelectric_circuitDirectEditVertical_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_DIODE && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_DIODE && alias == null) {
            return new Myelectric_circuitDirectEditVertical_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_ON_SWITCH && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH && alias == null) {
            return new Myelectric_circuitDirectEditVertical_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_OFF_SWITCH && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH && alias == null) {
            return new Myelectric_circuitDirectEditVertical_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSFORMATOR && alias == null) {
            return new Myelectric_circuitDirectEditVertical_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSISTOR && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR && alias == null) {
            return new Myelectric_circuitDirectEditVertical_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VOLTMETER && alias == null) {
            return new Myelectric_circuitDirectEditVoltmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.AMMETER && alias == null) {
            return new Myelectric_circuitDirectEditAmmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_FUSE && alias == null) {
            return new Myelectric_circuitDirectEditVertical_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_FUSE && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_THERMISTOR && alias == null) {
            return new Myelectric_circuitDirectEditVertical_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_THERMISTOR && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_VARIABEL_RESISTOR && alias == null) {
            return new Myelectric_circuitDirectEditVertical_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_VARIABEL_RESISTOR && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_INDUCTOR && alias == null) {
            return new Myelectric_circuitDirectEditVertical_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_INDUCTOR && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_CAPACITOR && alias == null) {
            return new Myelectric_circuitDirectEditVertical_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_CAPACITOR && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_AMPLIFIER && alias == null) {
            return new Myelectric_circuitDirectEditVertical_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_AMPLIFIER && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.MOTOR && alias == null) {
            return new Myelectric_circuitDirectEditMotorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.GROUND && alias == null) {
            return new Myelectric_circuitDirectEditGroundFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_LOUDSPEAKER && alias == null) {
            return new Myelectric_circuitDirectEditHorizontal_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER && alias == null) {
            return new Myelectric_circuitDirectEditVertical_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.CABEL_CORNER && alias == null) {
            return new Myelectric_circuitDirectEditCabel_CornerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.WIRE && alias == null) {
            return new Myelectric_circuitDirectEditWireFeature(this);
        }
        return super.getDirectEditingFeature(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICustomFeature[] getCustomFeatures(final ICustomContext context) {
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(context.getPictogramElements()[0]);
        if (bo == null) {
            return new ICustomFeature[0];
        }
        final String alias = GraphitiProperties.get(context.getPictogramElements()[0], PROPERTY_ALIAS);
        return new ICustomFeature[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IResizeShapeFeature getResizeShapeFeature(IResizeShapeContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final EObject bo = (EObject) getBusinessObjectForPictogramElement(pictogramElement);
        if (bo == null) {
            return null;
        }
        final String alias = peService.getPropertyValue(pictogramElement, PROPERTY_ALIAS);
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.LAMP && alias == null) {
            return new Myelectric_circuitResizeLampFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_SOURCE && alias == null) {
            return new Myelectric_circuitResizeHorizontal_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_SOURCE && alias == null) {
            return new Myelectric_circuitResizeVertical_SourceFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_RESISTOR && alias == null) {
            return new Myelectric_circuitResizeHorizontal_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_RESISTOR && alias == null) {
            return new Myelectric_circuitResizeVertical_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_DIODE && alias == null) {
            return new Myelectric_circuitResizeHorizontal_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_DIODE && alias == null) {
            return new Myelectric_circuitResizeVertical_DiodeFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_ON_SWITCH && alias == null) {
            return new Myelectric_circuitResizeHorizontal_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH && alias == null) {
            return new Myelectric_circuitResizeVertical_On_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_OFF_SWITCH && alias == null) {
            return new Myelectric_circuitResizeHorizontal_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH && alias == null) {
            return new Myelectric_circuitResizeVertical_Off_SwitchFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSFORMATOR && alias == null) {
            return new Myelectric_circuitResizeVertical_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR && alias == null) {
            return new Myelectric_circuitResizeHorizontal_TransformatorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSISTOR && alias == null) {
            return new Myelectric_circuitResizeHorizontal_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR && alias == null) {
            return new Myelectric_circuitResizeVertical_TransistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VOLTMETER && alias == null) {
            return new Myelectric_circuitResizeVoltmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.AMMETER && alias == null) {
            return new Myelectric_circuitResizeAmmeterFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_FUSE && alias == null) {
            return new Myelectric_circuitResizeVertical_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_FUSE && alias == null) {
            return new Myelectric_circuitResizeHorizontal_FuseFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_THERMISTOR && alias == null) {
            return new Myelectric_circuitResizeVertical_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_THERMISTOR && alias == null) {
            return new Myelectric_circuitResizeHorizontal_ThermistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_VARIABEL_RESISTOR && alias == null) {
            return new Myelectric_circuitResizeVertical_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_VARIABEL_RESISTOR && alias == null) {
            return new Myelectric_circuitResizeHorizontal_Variabel_ResistorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_INDUCTOR && alias == null) {
            return new Myelectric_circuitResizeVertical_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_INDUCTOR && alias == null) {
            return new Myelectric_circuitResizeHorizontal_InductorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_CAPACITOR && alias == null) {
            return new Myelectric_circuitResizeVertical_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_CAPACITOR && alias == null) {
            return new Myelectric_circuitResizeHorizontal_CapacitorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_AMPLIFIER && alias == null) {
            return new Myelectric_circuitResizeVertical_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_AMPLIFIER && alias == null) {
            return new Myelectric_circuitResizeHorizontal_AmplifierFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.MOTOR && alias == null) {
            return new Myelectric_circuitResizeMotorFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.GROUND && alias == null) {
            return new Myelectric_circuitResizeGroundFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_LOUDSPEAKER && alias == null) {
            return new Myelectric_circuitResizeHorizontal_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER && alias == null) {
            return new Myelectric_circuitResizeVertical_LoudspeakerFeature(this);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.CABEL_CORNER && alias == null) {
            return new Myelectric_circuitResizeCabel_CornerFeature(this);
        }
        return super.getResizeShapeFeature(context);
    }
}
