/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:33 CET 2015 by Spray UpdateShapeFromDslFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.IGaService;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractUpdateFeature;
import org.eclipselabs.spray.runtime.graphiti.layout.SprayLayoutService;
import org.eclipselabs.spray.examples.electric_circuit.Activator;

public abstract class Myelectric_circuitUpdateHorizontal_Variabel_ResistorFeatureBase extends AbstractUpdateFeature {
    public Myelectric_circuitUpdateHorizontal_Variabel_ResistorFeatureBase(final IFeatureProvider fp) {
        super(fp);
        gaService = Activator.get(IGaService.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canUpdate(final IUpdateContext context) {
        // return true, if linked business object is a Electric_Plan_DSL.Horizontal_Variabel_Resistor
        final PictogramElement pictogramElement = context.getPictogramElement();
        final Object bo = getBusinessObjectForPictogramElement(pictogramElement);
        return (bo instanceof Electric_Plan_DSL.Horizontal_Variabel_Resistor) && (!(pictogramElement instanceof Diagram));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IReason updateNeeded(final IUpdateContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final Object bo = getBusinessObjectForPictogramElement(pictogramElement);
        if (!(bo instanceof Electric_Plan_DSL.Horizontal_Variabel_Resistor)) {
            return Reason.createFalseReason();
        }
        if (pictogramElement instanceof Shape) {
            Shape shape = (Shape) pictogramElement;
            Electric_Plan_DSL.Horizontal_Variabel_Resistor eClass = (Electric_Plan_DSL.Horizontal_Variabel_Resistor) bo;
            if (checkUpdateNeededRecursively(shape, eClass)) {
                return Reason.createTrueReason();
            }
            if (shape instanceof ContainerShape) {
                for (Shape childShape : ((ContainerShape) shape).getChildren()) {
                    if (checkUpdateNeededRecursively(childShape, eClass)) {
                        return Reason.createTrueReason();
                    }
                }
            }
        }
        return Reason.createFalseReason();
    }

    protected boolean checkUpdateNeededRecursively(Shape shape, final Electric_Plan_DSL.Horizontal_Variabel_Resistor eClass) {
        GraphicsAlgorithm graphicsAlgorithm = shape.getGraphicsAlgorithm();
        if (graphicsAlgorithm instanceof Text) {
            Text text = (Text) graphicsAlgorithm;
            String id = peService.getPropertyValue(graphicsAlgorithm, TEXT_ID);
            if (id != null) {
                if (id.equals("HorizontalVariabelResistorText")) {
                    String eClassValue = eClass.getCircuit_name();
                    String gAlgorithmValue = text.getValue();
                    if (eClassValue != null) {
                        if (!eClassValue.equals(gAlgorithmValue)) {
                            return true;
                        }
                    }
                }
            }
        }
        if (shape instanceof ContainerShape) {
            for (Shape child : ((ContainerShape) shape).getChildren()) {
                if (checkUpdateNeededRecursively(child, eClass)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean update(final IUpdateContext context) {
        final PictogramElement pictogramElement = context.getPictogramElement();
        final Electric_Plan_DSL.Horizontal_Variabel_Resistor eClass = (Electric_Plan_DSL.Horizontal_Variabel_Resistor) getBusinessObjectForPictogramElement(pictogramElement);
        if (pictogramElement instanceof Shape) {
            Shape shape = (Shape) pictogramElement;
            updateChildsRecursively(shape, eClass);
            Shape top = findTopShape(shape);
            SprayLayoutService.getLayoutManager(top).layout();
        }
        return true;

    }

    protected void updateChildsRecursively(Shape shape, final Electric_Plan_DSL.Horizontal_Variabel_Resistor eClass) {
        GraphicsAlgorithm graphicsAlgorithm = shape.getGraphicsAlgorithm();
        if (graphicsAlgorithm instanceof Text) {
            Text text = (Text) graphicsAlgorithm;
            String id = peService.getPropertyValue(graphicsAlgorithm, TEXT_ID);
            if (id != null) {
                if (id.equals("HorizontalVariabelResistorText")) {
                    text.setValue(eClass.getCircuit_name());
                    setDoneChanges(true);
                }
            }
        }

        if (shape instanceof ContainerShape) {
            for (Shape child : ((ContainerShape) shape).getChildren()) {
                updateChildsRecursively(child, eClass);
            }
        }
    }
}
