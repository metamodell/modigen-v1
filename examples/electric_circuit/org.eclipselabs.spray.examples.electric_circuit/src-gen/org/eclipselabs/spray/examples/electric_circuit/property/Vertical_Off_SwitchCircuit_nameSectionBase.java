/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:34 CET 2015 by Spray PropertySection.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.property;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.graphiti.ui.internal.services.GraphitiUiInternal;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;
import java.util.List;
import Electric_Plan_DSL.Circuit_Plan;

public abstract class Vertical_Off_SwitchCircuit_nameSectionBase extends GFPropertySection implements ITabbedPropertyConstants {

    protected Electric_Plan_DSL.Circuit_Plan bc = null;
    protected Text                           circuit_nameWidget;

    /**
     * {@inheritDoc}
     */
    @Override
    public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
        super.createControls(parent, tabbedPropertySheetPage);

        final TabbedPropertySheetWidgetFactory factory = getWidgetFactory();
        final Composite composite = factory.createFlatFormComposite(parent);
        FormData data;

        circuit_nameWidget = factory.createText(composite, "");
        data = new FormData();
        data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, VSPACE);
        circuit_nameWidget.setLayoutData(data);

        CLabel valueLabel = factory.createCLabel(composite, "Circuit_name");
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(circuit_nameWidget, -HSPACE);
        data.top = new FormAttachment(circuit_nameWidget, 0, SWT.CENTER);
        valueLabel.setLayoutData(data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
        circuit_nameWidget.removeModifyListener(nameListener);

        final PictogramElement pe = getSelectedPictogramElement();
        if (pe != null) {
            final EObject bo = (EObject) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
            // the filter assured, that it is a Circuit_Plan
            if (bo == null)
                return;
            bc = (Electric_Plan_DSL.Circuit_Plan) bo;
            String value = "";
            value = bc.getCircuit_name();
            circuit_nameWidget.setText(value == null ? "" : value);
            circuit_nameWidget.addModifyListener(nameListener);
        }
    }

    private ModifyListener nameListener = new ModifyListener() {
                                            public void modifyText(ModifyEvent arg0) {
                                                TransactionalEditingDomain editingDomain = getDiagramContainer().getDiagramBehavior().getEditingDomain();
                                                editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
                                                    @Override
                                                    protected void doExecute() {
                                                        changePropertyValue();
                                                    }
                                                });
                                            }
                                        };

    protected void changePropertyValue() {
        String newValue = circuit_nameWidget.getText();
        if (!newValue.equals(bc.getCircuit_name())) {
            bc.setCircuit_name(newValue);
        }
    }

}
