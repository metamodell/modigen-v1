/*************************************************************************************
 *
 * Generated on Mon Jan 05 17:46:35 CET 2015 by Spray PasteFeature.xtend
 *
 * This file contains generated and should not be changed.
 * Use the extension point class (the direct subclass of this class) to add manual code
 *
 *************************************************************************************/
package org.eclipselabs.spray.examples.electric_circuit.features;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IPasteContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.mm.Property;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipselabs.spray.runtime.graphiti.features.AbstractPasteFeature;
import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import org.eclipselabs.spray.examples.electric_circuit.diagram.Myelectric_circuitModelService;

public abstract class Myelectric_circuitPasteFeatureBase extends AbstractPasteFeature {

    protected Myelectric_circuitModelService modelService;

    public Myelectric_circuitPasteFeatureBase(IFeatureProvider fp) {
        super(fp);
        modelService = Myelectric_circuitModelService.getModelService(fp.getDiagramTypeProvider());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canPaste(IPasteContext context) {
        // TODO: only support pasting directly in the diagram
        PictogramElement[] pes = context.getPictogramElements();
        if (pes.length != 1 || !(pes[0] instanceof Diagram)) {
            return false;
        }
        // can paste, if all objects on the clipboard are PictogramElements with link on subclasses of Electric_Plan_DSL.Circuit_Plan
        Object[] fromClipboard = getFromClipboard();
        if (fromClipboard == null || fromClipboard.length == 0) {
            return false;
        }
        for (Object object : fromClipboard) {
            if (!(object instanceof PictogramElement)) {
                return false;
            } else if (!(getBusinessObjectForPictogramElement((PictogramElement) object) instanceof Electric_Plan_DSL.Circuit_Plan)) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paste(IPasteContext context) {
        // already verified, that pasting is allowed just directly in the diagram
        PictogramElement[] pes = context.getPictogramElements();
        Diagram diagram = (Diagram) pes[0];

        // get the PictogramElements from the clipboard and the linked business object.
        Object[] objects = getFromClipboard();
        for (Object object : objects) {
            PictogramElement pictogramElement = (PictogramElement) object;
            Electric_Plan_DSL.Circuit_Plan boRef = (Electric_Plan_DSL.Circuit_Plan) getBusinessObjectForPictogramElement(pictogramElement);
            Electric_Plan_DSL.Circuit_Plan bo = EcoreUtil.copy(boRef);
            addBusinessObjectToContainer(bo, pictogramElement);

            // create a new AddContext for the creation of a new shape.
            AddContext ac = new AddContext(new AddContext(), bo);
            ac.setLocation(0, 0); // for simplicity paste at (0, 0)
            ac.setTargetContainer(diagram); // paste on diagram
            // copy all properties from the shape (e.g. ALIAS etc.)
            for (Property prop : pictogramElement.getProperties()) {
                ac.putProperty(prop.getKey(), prop.getValue());
            }
            getFeatureProvider().addIfPossible(ac);
        }
    }

    protected void addBusinessObjectToContainer(Electric_Plan_DSL.Circuit_Plan bo, PictogramElement pe) {
        final Electric_Plan_DSL.Circuit_Plan model = modelService.getModel();
        final String alias = Graphiti.getPeService().getPropertyValue(pe, PROPERTY_ALIAS);
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.LAMP && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Lamp) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_SOURCE && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Source) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_SOURCE && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Source) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_RESISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Resistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_RESISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Resistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_DIODE && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Diode) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_DIODE && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Diode) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_ON_SWITCH && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_On_Switch) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_On_Switch) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_OFF_SWITCH && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Off_Switch) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Off_Switch) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSFORMATOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Transformator) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Transformator) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Transistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Transistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VOLTMETER && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Voltmeter) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.AMMETER && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Ammeter) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_FUSE && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Fuse) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_FUSE && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Fuse) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_THERMISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Thermistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_THERMISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Thermistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_VARIABEL_RESISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Variabel_Resistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_VARIABEL_RESISTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Variabel_Resistor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_INDUCTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Inductor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_INDUCTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Inductor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_CAPACITOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Capacitor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_CAPACITOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Capacitor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_AMPLIFIER && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Amplifier) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_AMPLIFIER && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Amplifier) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.MOTOR && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Motor) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.GROUND && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Ground) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.HORIZONTAL_LOUDSPEAKER && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Horizontal_Loudspeaker) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Vertical_Loudspeaker) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.CABEL_CORNER && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Cabel_Corner) bo);
        }
        if (bo.eClass() == Electric_Plan_DSLPackage.Literals.WIRE && alias == null) {
            model.getCircuit_referenz().add((Electric_Plan_DSL.Wire) bo);
        }
    }
}
