/**
 */
package Electric_Plan_DSL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Circuit Plan</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_name <em>Circuit name</em>}</li>
 *   <li>{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_description <em>Circuit description</em>}</li>
 *   <li>{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_referenz <em>Circuit referenz</em>}</li>
 * </ul>
 * </p>
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getCircuit_Plan()
 * @model
 * @generated
 */
public interface Circuit_Plan extends EObject {
	/**
	 * Returns the value of the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Circuit name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Circuit name</em>' attribute.
	 * @see #setCircuit_name(String)
	 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getCircuit_Plan_Circuit_name()
	 * @model
	 * @generated
	 */
	String getCircuit_name();

	/**
	 * Sets the value of the '{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_name <em>Circuit name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Circuit name</em>' attribute.
	 * @see #getCircuit_name()
	 * @generated
	 */
	void setCircuit_name(String value);

	/**
	 * Returns the value of the '<em><b>Circuit description</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Circuit description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Circuit description</em>' attribute.
	 * @see #setCircuit_description(String)
	 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getCircuit_Plan_Circuit_description()
	 * @model default=""
	 * @generated
	 */
	String getCircuit_description();

	/**
	 * Sets the value of the '{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_description <em>Circuit description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Circuit description</em>' attribute.
	 * @see #getCircuit_description()
	 * @generated
	 */
	void setCircuit_description(String value);

	/**
	 * Returns the value of the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * The list contents are of type {@link Electric_Plan_DSL.Circuit_Plan}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Circuit referenz</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Circuit referenz</em>' containment reference list.
	 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getCircuit_Plan_Circuit_referenz()
	 * @model containment="true"
	 * @generated
	 */
	EList<Circuit_Plan> getCircuit_referenz();

} // Circuit_Plan
