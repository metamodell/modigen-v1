/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wire</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Electric_Plan_DSL.Wire#getFromComponent <em>From Component</em>}</li>
 *   <li>{@link Electric_Plan_DSL.Wire#getToComponent <em>To Component</em>}</li>
 * </ul>
 * </p>
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getWire()
 * @model
 * @generated
 */
public interface Wire extends Connection {
	/**
	 * Returns the value of the '<em><b>From Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Component</em>' reference.
	 * @see #setFromComponent(Component)
	 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getWire_FromComponent()
	 * @model
	 * @generated
	 */
	Component getFromComponent();

	/**
	 * Sets the value of the '{@link Electric_Plan_DSL.Wire#getFromComponent <em>From Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Component</em>' reference.
	 * @see #getFromComponent()
	 * @generated
	 */
	void setFromComponent(Component value);

	/**
	 * Returns the value of the '<em><b>To Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Component</em>' reference.
	 * @see #setToComponent(Component)
	 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getWire_ToComponent()
	 * @model ordered="false"
	 * @generated
	 */
	Component getToComponent();

	/**
	 * Sets the value of the '{@link Electric_Plan_DSL.Wire#getToComponent <em>To Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Component</em>' reference.
	 * @see #getToComponent()
	 * @generated
	 */
	void setToComponent(Component value);

} // Wire
