/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getConnection()
 * @model
 * @generated
 */
public interface Connection extends Circuit_Plan {
} // Connection
