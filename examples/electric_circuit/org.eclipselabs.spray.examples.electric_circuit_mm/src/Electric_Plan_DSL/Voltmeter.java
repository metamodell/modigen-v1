/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Voltmeter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVoltmeter()
 * @model
 * @generated
 */
public interface Voltmeter extends Component {
} // Voltmeter
