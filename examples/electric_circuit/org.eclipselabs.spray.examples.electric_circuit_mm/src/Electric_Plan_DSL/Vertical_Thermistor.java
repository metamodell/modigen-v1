/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Thermistor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Thermistor()
 * @model
 * @generated
 */
public interface Vertical_Thermistor extends Component {
} // Vertical_Thermistor
