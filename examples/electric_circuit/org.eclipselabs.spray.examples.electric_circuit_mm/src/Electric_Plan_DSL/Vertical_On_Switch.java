/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical On Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_On_Switch()
 * @model
 * @generated
 */
public interface Vertical_On_Switch extends Component {
} // Vertical_On_Switch
