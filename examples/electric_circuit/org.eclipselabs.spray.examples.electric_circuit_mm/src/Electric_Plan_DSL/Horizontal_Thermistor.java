/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Thermistor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getHorizontal_Thermistor()
 * @model
 * @generated
 */
public interface Horizontal_Thermistor extends Component {
} // Horizontal_Thermistor
