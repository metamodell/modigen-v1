/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal On Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getHorizontal_On_Switch()
 * @model
 * @generated
 */
public interface Horizontal_On_Switch extends Component {
} // Horizontal_On_Switch
