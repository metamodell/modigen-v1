/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Variabel Resistor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Variabel_Resistor()
 * @model
 * @generated
 */
public interface Vertical_Variabel_Resistor extends Component {
} // Vertical_Variabel_Resistor
