/**
 */
package Electric_Plan_DSL.util;

import Electric_Plan_DSL.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage
 * @generated
 */
public class Electric_Plan_DSLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Electric_Plan_DSLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Electric_Plan_DSLSwitch() {
		if (modelPackage == null) {
			modelPackage = Electric_Plan_DSLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN: {
				Circuit_Plan circuit_Plan = (Circuit_Plan)theEObject;
				T result = caseCircuit_Plan(circuit_Plan);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.COMPONENT: {
				Component component = (Component)theEObject;
				T result = caseComponent(component);
				if (result == null) result = caseCircuit_Plan(component);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.CONNECTION: {
				Connection connection = (Connection)theEObject;
				T result = caseConnection(connection);
				if (result == null) result = caseCircuit_Plan(connection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.LAMP: {
				Lamp lamp = (Lamp)theEObject;
				T result = caseLamp(lamp);
				if (result == null) result = caseComponent(lamp);
				if (result == null) result = caseCircuit_Plan(lamp);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_RESISTOR: {
				Vertical_Resistor vertical_Resistor = (Vertical_Resistor)theEObject;
				T result = caseVertical_Resistor(vertical_Resistor);
				if (result == null) result = caseComponent(vertical_Resistor);
				if (result == null) result = caseCircuit_Plan(vertical_Resistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_RESISTOR: {
				Horizontal_Resistor horizontal_Resistor = (Horizontal_Resistor)theEObject;
				T result = caseHorizontal_Resistor(horizontal_Resistor);
				if (result == null) result = caseComponent(horizontal_Resistor);
				if (result == null) result = caseCircuit_Plan(horizontal_Resistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_DIODE: {
				Vertical_Diode vertical_Diode = (Vertical_Diode)theEObject;
				T result = caseVertical_Diode(vertical_Diode);
				if (result == null) result = caseComponent(vertical_Diode);
				if (result == null) result = caseCircuit_Plan(vertical_Diode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_DIODE: {
				Horizontal_Diode horizontal_Diode = (Horizontal_Diode)theEObject;
				T result = caseHorizontal_Diode(horizontal_Diode);
				if (result == null) result = caseComponent(horizontal_Diode);
				if (result == null) result = caseCircuit_Plan(horizontal_Diode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_OFF_SWITCH: {
				Horizontal_Off_Switch horizontal_Off_Switch = (Horizontal_Off_Switch)theEObject;
				T result = caseHorizontal_Off_Switch(horizontal_Off_Switch);
				if (result == null) result = caseComponent(horizontal_Off_Switch);
				if (result == null) result = caseCircuit_Plan(horizontal_Off_Switch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_OFF_SWITCH: {
				Vertical_Off_Switch vertical_Off_Switch = (Vertical_Off_Switch)theEObject;
				T result = caseVertical_Off_Switch(vertical_Off_Switch);
				if (result == null) result = caseComponent(vertical_Off_Switch);
				if (result == null) result = caseCircuit_Plan(vertical_Off_Switch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_ON_SWITCH: {
				Horizontal_On_Switch horizontal_On_Switch = (Horizontal_On_Switch)theEObject;
				T result = caseHorizontal_On_Switch(horizontal_On_Switch);
				if (result == null) result = caseComponent(horizontal_On_Switch);
				if (result == null) result = caseCircuit_Plan(horizontal_On_Switch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_ON_SWITCH: {
				Vertical_On_Switch vertical_On_Switch = (Vertical_On_Switch)theEObject;
				T result = caseVertical_On_Switch(vertical_On_Switch);
				if (result == null) result = caseComponent(vertical_On_Switch);
				if (result == null) result = caseCircuit_Plan(vertical_On_Switch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_SOURCE: {
				Horizontal_Source horizontal_Source = (Horizontal_Source)theEObject;
				T result = caseHorizontal_Source(horizontal_Source);
				if (result == null) result = caseComponent(horizontal_Source);
				if (result == null) result = caseCircuit_Plan(horizontal_Source);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_SOURCE: {
				Vertical_Source vertical_Source = (Vertical_Source)theEObject;
				T result = caseVertical_Source(vertical_Source);
				if (result == null) result = caseComponent(vertical_Source);
				if (result == null) result = caseCircuit_Plan(vertical_Source);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_TRANSFORMATOR: {
				Vertical_Transformator vertical_Transformator = (Vertical_Transformator)theEObject;
				T result = caseVertical_Transformator(vertical_Transformator);
				if (result == null) result = caseComponent(vertical_Transformator);
				if (result == null) result = caseCircuit_Plan(vertical_Transformator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_TRANSFORMATOR: {
				Horizontal_Transformator horizontal_Transformator = (Horizontal_Transformator)theEObject;
				T result = caseHorizontal_Transformator(horizontal_Transformator);
				if (result == null) result = caseComponent(horizontal_Transformator);
				if (result == null) result = caseCircuit_Plan(horizontal_Transformator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_TRANSISTOR: {
				Vertical_Transistor vertical_Transistor = (Vertical_Transistor)theEObject;
				T result = caseVertical_Transistor(vertical_Transistor);
				if (result == null) result = caseComponent(vertical_Transistor);
				if (result == null) result = caseCircuit_Plan(vertical_Transistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_TRANSISTOR: {
				Horizontal_Transistor horizontal_Transistor = (Horizontal_Transistor)theEObject;
				T result = caseHorizontal_Transistor(horizontal_Transistor);
				if (result == null) result = caseComponent(horizontal_Transistor);
				if (result == null) result = caseCircuit_Plan(horizontal_Transistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VOLTMETER: {
				Voltmeter voltmeter = (Voltmeter)theEObject;
				T result = caseVoltmeter(voltmeter);
				if (result == null) result = caseComponent(voltmeter);
				if (result == null) result = caseCircuit_Plan(voltmeter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.AMMETER: {
				Ammeter ammeter = (Ammeter)theEObject;
				T result = caseAmmeter(ammeter);
				if (result == null) result = caseComponent(ammeter);
				if (result == null) result = caseCircuit_Plan(ammeter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_FUSE: {
				Vertical_Fuse vertical_Fuse = (Vertical_Fuse)theEObject;
				T result = caseVertical_Fuse(vertical_Fuse);
				if (result == null) result = caseComponent(vertical_Fuse);
				if (result == null) result = caseCircuit_Plan(vertical_Fuse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_FUSE: {
				Horizontal_Fuse horizontal_Fuse = (Horizontal_Fuse)theEObject;
				T result = caseHorizontal_Fuse(horizontal_Fuse);
				if (result == null) result = caseComponent(horizontal_Fuse);
				if (result == null) result = caseCircuit_Plan(horizontal_Fuse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_THERMISTOR: {
				Vertical_Thermistor vertical_Thermistor = (Vertical_Thermistor)theEObject;
				T result = caseVertical_Thermistor(vertical_Thermistor);
				if (result == null) result = caseComponent(vertical_Thermistor);
				if (result == null) result = caseCircuit_Plan(vertical_Thermistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_THERMISTOR: {
				Horizontal_Thermistor horizontal_Thermistor = (Horizontal_Thermistor)theEObject;
				T result = caseHorizontal_Thermistor(horizontal_Thermistor);
				if (result == null) result = caseComponent(horizontal_Thermistor);
				if (result == null) result = caseCircuit_Plan(horizontal_Thermistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_VARIABEL_RESISTOR: {
				Vertical_Variabel_Resistor vertical_Variabel_Resistor = (Vertical_Variabel_Resistor)theEObject;
				T result = caseVertical_Variabel_Resistor(vertical_Variabel_Resistor);
				if (result == null) result = caseComponent(vertical_Variabel_Resistor);
				if (result == null) result = caseCircuit_Plan(vertical_Variabel_Resistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_VARIABEL_RESISTOR: {
				Horizontal_Variabel_Resistor horizontal_Variabel_Resistor = (Horizontal_Variabel_Resistor)theEObject;
				T result = caseHorizontal_Variabel_Resistor(horizontal_Variabel_Resistor);
				if (result == null) result = caseComponent(horizontal_Variabel_Resistor);
				if (result == null) result = caseCircuit_Plan(horizontal_Variabel_Resistor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_INDUCTOR: {
				Vertical_Inductor vertical_Inductor = (Vertical_Inductor)theEObject;
				T result = caseVertical_Inductor(vertical_Inductor);
				if (result == null) result = caseComponent(vertical_Inductor);
				if (result == null) result = caseCircuit_Plan(vertical_Inductor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_INDUCTOR: {
				Horizontal_Inductor horizontal_Inductor = (Horizontal_Inductor)theEObject;
				T result = caseHorizontal_Inductor(horizontal_Inductor);
				if (result == null) result = caseComponent(horizontal_Inductor);
				if (result == null) result = caseCircuit_Plan(horizontal_Inductor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_CAPACITOR: {
				Vertical_Capacitor vertical_Capacitor = (Vertical_Capacitor)theEObject;
				T result = caseVertical_Capacitor(vertical_Capacitor);
				if (result == null) result = caseComponent(vertical_Capacitor);
				if (result == null) result = caseCircuit_Plan(vertical_Capacitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_CAPACITOR: {
				Horizontal_Capacitor horizontal_Capacitor = (Horizontal_Capacitor)theEObject;
				T result = caseHorizontal_Capacitor(horizontal_Capacitor);
				if (result == null) result = caseComponent(horizontal_Capacitor);
				if (result == null) result = caseCircuit_Plan(horizontal_Capacitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_AMPLIFIER: {
				Vertical_Amplifier vertical_Amplifier = (Vertical_Amplifier)theEObject;
				T result = caseVertical_Amplifier(vertical_Amplifier);
				if (result == null) result = caseComponent(vertical_Amplifier);
				if (result == null) result = caseCircuit_Plan(vertical_Amplifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_AMPLIFIER: {
				Horizontal_Amplifier horizontal_Amplifier = (Horizontal_Amplifier)theEObject;
				T result = caseHorizontal_Amplifier(horizontal_Amplifier);
				if (result == null) result = caseComponent(horizontal_Amplifier);
				if (result == null) result = caseCircuit_Plan(horizontal_Amplifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.MOTOR: {
				Motor motor = (Motor)theEObject;
				T result = caseMotor(motor);
				if (result == null) result = caseComponent(motor);
				if (result == null) result = caseCircuit_Plan(motor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.GROUND: {
				Ground ground = (Ground)theEObject;
				T result = caseGround(ground);
				if (result == null) result = caseComponent(ground);
				if (result == null) result = caseCircuit_Plan(ground);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.HORIZONTAL_LOUDSPEAKER: {
				Horizontal_Loudspeaker horizontal_Loudspeaker = (Horizontal_Loudspeaker)theEObject;
				T result = caseHorizontal_Loudspeaker(horizontal_Loudspeaker);
				if (result == null) result = caseComponent(horizontal_Loudspeaker);
				if (result == null) result = caseCircuit_Plan(horizontal_Loudspeaker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.VERTICAL_LOUDSPEAKER: {
				Vertical_Loudspeaker vertical_Loudspeaker = (Vertical_Loudspeaker)theEObject;
				T result = caseVertical_Loudspeaker(vertical_Loudspeaker);
				if (result == null) result = caseComponent(vertical_Loudspeaker);
				if (result == null) result = caseCircuit_Plan(vertical_Loudspeaker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.CABEL_CORNER: {
				Cabel_Corner cabel_Corner = (Cabel_Corner)theEObject;
				T result = caseCabel_Corner(cabel_Corner);
				if (result == null) result = caseComponent(cabel_Corner);
				if (result == null) result = caseCircuit_Plan(cabel_Corner);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case Electric_Plan_DSLPackage.WIRE: {
				Wire wire = (Wire)theEObject;
				T result = caseWire(wire);
				if (result == null) result = caseConnection(wire);
				if (result == null) result = caseCircuit_Plan(wire);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Circuit Plan</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Circuit Plan</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCircuit_Plan(Circuit_Plan object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponent(Component object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnection(Connection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lamp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lamp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLamp(Lamp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Resistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Resistor(Vertical_Resistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Resistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Resistor(Horizontal_Resistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Diode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Diode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Diode(Vertical_Diode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Diode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Diode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Diode(Horizontal_Diode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Off Switch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Off Switch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Off_Switch(Horizontal_Off_Switch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Off Switch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Off Switch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Off_Switch(Vertical_Off_Switch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal On Switch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal On Switch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_On_Switch(Horizontal_On_Switch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical On Switch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical On Switch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_On_Switch(Vertical_On_Switch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Source(Horizontal_Source object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Source(Vertical_Source object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Transformator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Transformator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Transformator(Vertical_Transformator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Transformator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Transformator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Transformator(Horizontal_Transformator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Transistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Transistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Transistor(Vertical_Transistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Transistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Transistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Transistor(Horizontal_Transistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Voltmeter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Voltmeter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVoltmeter(Voltmeter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ammeter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ammeter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAmmeter(Ammeter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Fuse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Fuse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Fuse(Vertical_Fuse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Fuse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Fuse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Fuse(Horizontal_Fuse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Thermistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Thermistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Thermistor(Vertical_Thermistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Thermistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Thermistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Thermistor(Horizontal_Thermistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Variabel Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Variabel Resistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Variabel_Resistor(Vertical_Variabel_Resistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Variabel Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Variabel Resistor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Variabel_Resistor(Horizontal_Variabel_Resistor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Inductor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Inductor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Inductor(Vertical_Inductor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Inductor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Inductor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Inductor(Horizontal_Inductor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Capacitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Capacitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Capacitor(Vertical_Capacitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Capacitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Capacitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Capacitor(Horizontal_Capacitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Amplifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Amplifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Amplifier(Vertical_Amplifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Amplifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Amplifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Amplifier(Horizontal_Amplifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Motor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Motor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMotor(Motor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ground</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ground</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGround(Ground object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Loudspeaker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Loudspeaker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontal_Loudspeaker(Horizontal_Loudspeaker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Loudspeaker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Loudspeaker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVertical_Loudspeaker(Vertical_Loudspeaker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cabel Corner</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cabel Corner</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCabel_Corner(Cabel_Corner object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wire</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wire</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWire(Wire object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //Electric_Plan_DSLSwitch
