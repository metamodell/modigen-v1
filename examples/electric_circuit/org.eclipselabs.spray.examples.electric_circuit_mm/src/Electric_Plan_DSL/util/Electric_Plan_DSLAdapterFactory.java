/**
 */
package Electric_Plan_DSL.util;

import Electric_Plan_DSL.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage
 * @generated
 */
public class Electric_Plan_DSLAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Electric_Plan_DSLPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Electric_Plan_DSLAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Electric_Plan_DSLPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Electric_Plan_DSLSwitch<Adapter> modelSwitch =
		new Electric_Plan_DSLSwitch<Adapter>() {
			@Override
			public Adapter caseCircuit_Plan(Circuit_Plan object) {
				return createCircuit_PlanAdapter();
			}
			@Override
			public Adapter caseComponent(Component object) {
				return createComponentAdapter();
			}
			@Override
			public Adapter caseConnection(Connection object) {
				return createConnectionAdapter();
			}
			@Override
			public Adapter caseLamp(Lamp object) {
				return createLampAdapter();
			}
			@Override
			public Adapter caseVertical_Resistor(Vertical_Resistor object) {
				return createVertical_ResistorAdapter();
			}
			@Override
			public Adapter caseHorizontal_Resistor(Horizontal_Resistor object) {
				return createHorizontal_ResistorAdapter();
			}
			@Override
			public Adapter caseVertical_Diode(Vertical_Diode object) {
				return createVertical_DiodeAdapter();
			}
			@Override
			public Adapter caseHorizontal_Diode(Horizontal_Diode object) {
				return createHorizontal_DiodeAdapter();
			}
			@Override
			public Adapter caseHorizontal_Off_Switch(Horizontal_Off_Switch object) {
				return createHorizontal_Off_SwitchAdapter();
			}
			@Override
			public Adapter caseVertical_Off_Switch(Vertical_Off_Switch object) {
				return createVertical_Off_SwitchAdapter();
			}
			@Override
			public Adapter caseHorizontal_On_Switch(Horizontal_On_Switch object) {
				return createHorizontal_On_SwitchAdapter();
			}
			@Override
			public Adapter caseVertical_On_Switch(Vertical_On_Switch object) {
				return createVertical_On_SwitchAdapter();
			}
			@Override
			public Adapter caseHorizontal_Source(Horizontal_Source object) {
				return createHorizontal_SourceAdapter();
			}
			@Override
			public Adapter caseVertical_Source(Vertical_Source object) {
				return createVertical_SourceAdapter();
			}
			@Override
			public Adapter caseVertical_Transformator(Vertical_Transformator object) {
				return createVertical_TransformatorAdapter();
			}
			@Override
			public Adapter caseHorizontal_Transformator(Horizontal_Transformator object) {
				return createHorizontal_TransformatorAdapter();
			}
			@Override
			public Adapter caseVertical_Transistor(Vertical_Transistor object) {
				return createVertical_TransistorAdapter();
			}
			@Override
			public Adapter caseHorizontal_Transistor(Horizontal_Transistor object) {
				return createHorizontal_TransistorAdapter();
			}
			@Override
			public Adapter caseVoltmeter(Voltmeter object) {
				return createVoltmeterAdapter();
			}
			@Override
			public Adapter caseAmmeter(Ammeter object) {
				return createAmmeterAdapter();
			}
			@Override
			public Adapter caseVertical_Fuse(Vertical_Fuse object) {
				return createVertical_FuseAdapter();
			}
			@Override
			public Adapter caseHorizontal_Fuse(Horizontal_Fuse object) {
				return createHorizontal_FuseAdapter();
			}
			@Override
			public Adapter caseVertical_Thermistor(Vertical_Thermistor object) {
				return createVertical_ThermistorAdapter();
			}
			@Override
			public Adapter caseHorizontal_Thermistor(Horizontal_Thermistor object) {
				return createHorizontal_ThermistorAdapter();
			}
			@Override
			public Adapter caseVertical_Variabel_Resistor(Vertical_Variabel_Resistor object) {
				return createVertical_Variabel_ResistorAdapter();
			}
			@Override
			public Adapter caseHorizontal_Variabel_Resistor(Horizontal_Variabel_Resistor object) {
				return createHorizontal_Variabel_ResistorAdapter();
			}
			@Override
			public Adapter caseVertical_Inductor(Vertical_Inductor object) {
				return createVertical_InductorAdapter();
			}
			@Override
			public Adapter caseHorizontal_Inductor(Horizontal_Inductor object) {
				return createHorizontal_InductorAdapter();
			}
			@Override
			public Adapter caseVertical_Capacitor(Vertical_Capacitor object) {
				return createVertical_CapacitorAdapter();
			}
			@Override
			public Adapter caseHorizontal_Capacitor(Horizontal_Capacitor object) {
				return createHorizontal_CapacitorAdapter();
			}
			@Override
			public Adapter caseVertical_Amplifier(Vertical_Amplifier object) {
				return createVertical_AmplifierAdapter();
			}
			@Override
			public Adapter caseHorizontal_Amplifier(Horizontal_Amplifier object) {
				return createHorizontal_AmplifierAdapter();
			}
			@Override
			public Adapter caseMotor(Motor object) {
				return createMotorAdapter();
			}
			@Override
			public Adapter caseGround(Ground object) {
				return createGroundAdapter();
			}
			@Override
			public Adapter caseHorizontal_Loudspeaker(Horizontal_Loudspeaker object) {
				return createHorizontal_LoudspeakerAdapter();
			}
			@Override
			public Adapter caseVertical_Loudspeaker(Vertical_Loudspeaker object) {
				return createVertical_LoudspeakerAdapter();
			}
			@Override
			public Adapter caseCabel_Corner(Cabel_Corner object) {
				return createCabel_CornerAdapter();
			}
			@Override
			public Adapter caseWire(Wire object) {
				return createWireAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Circuit_Plan <em>Circuit Plan</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Circuit_Plan
	 * @generated
	 */
	public Adapter createCircuit_PlanAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Component
	 * @generated
	 */
	public Adapter createComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Connection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Connection
	 * @generated
	 */
	public Adapter createConnectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Lamp <em>Lamp</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Lamp
	 * @generated
	 */
	public Adapter createLampAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Resistor <em>Vertical Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Resistor
	 * @generated
	 */
	public Adapter createVertical_ResistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Resistor <em>Horizontal Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Resistor
	 * @generated
	 */
	public Adapter createHorizontal_ResistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Diode <em>Vertical Diode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Diode
	 * @generated
	 */
	public Adapter createVertical_DiodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Diode <em>Horizontal Diode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Diode
	 * @generated
	 */
	public Adapter createHorizontal_DiodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Off_Switch <em>Horizontal Off Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Off_Switch
	 * @generated
	 */
	public Adapter createHorizontal_Off_SwitchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Off_Switch <em>Vertical Off Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Off_Switch
	 * @generated
	 */
	public Adapter createVertical_Off_SwitchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_On_Switch <em>Horizontal On Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_On_Switch
	 * @generated
	 */
	public Adapter createHorizontal_On_SwitchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_On_Switch <em>Vertical On Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_On_Switch
	 * @generated
	 */
	public Adapter createVertical_On_SwitchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Source <em>Horizontal Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Source
	 * @generated
	 */
	public Adapter createHorizontal_SourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Source <em>Vertical Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Source
	 * @generated
	 */
	public Adapter createVertical_SourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Transformator <em>Vertical Transformator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Transformator
	 * @generated
	 */
	public Adapter createVertical_TransformatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Transformator <em>Horizontal Transformator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Transformator
	 * @generated
	 */
	public Adapter createHorizontal_TransformatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Transistor <em>Vertical Transistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Transistor
	 * @generated
	 */
	public Adapter createVertical_TransistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Transistor <em>Horizontal Transistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Transistor
	 * @generated
	 */
	public Adapter createHorizontal_TransistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Voltmeter <em>Voltmeter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Voltmeter
	 * @generated
	 */
	public Adapter createVoltmeterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Ammeter <em>Ammeter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Ammeter
	 * @generated
	 */
	public Adapter createAmmeterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Fuse <em>Vertical Fuse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Fuse
	 * @generated
	 */
	public Adapter createVertical_FuseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Fuse <em>Horizontal Fuse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Fuse
	 * @generated
	 */
	public Adapter createHorizontal_FuseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Thermistor <em>Vertical Thermistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Thermistor
	 * @generated
	 */
	public Adapter createVertical_ThermistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Thermistor <em>Horizontal Thermistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Thermistor
	 * @generated
	 */
	public Adapter createHorizontal_ThermistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Variabel_Resistor <em>Vertical Variabel Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Variabel_Resistor
	 * @generated
	 */
	public Adapter createVertical_Variabel_ResistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Variabel_Resistor <em>Horizontal Variabel Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Variabel_Resistor
	 * @generated
	 */
	public Adapter createHorizontal_Variabel_ResistorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Inductor <em>Vertical Inductor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Inductor
	 * @generated
	 */
	public Adapter createVertical_InductorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Inductor <em>Horizontal Inductor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Inductor
	 * @generated
	 */
	public Adapter createHorizontal_InductorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Capacitor <em>Vertical Capacitor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Capacitor
	 * @generated
	 */
	public Adapter createVertical_CapacitorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Capacitor <em>Horizontal Capacitor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Capacitor
	 * @generated
	 */
	public Adapter createHorizontal_CapacitorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Amplifier <em>Vertical Amplifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Amplifier
	 * @generated
	 */
	public Adapter createVertical_AmplifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Amplifier <em>Horizontal Amplifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Amplifier
	 * @generated
	 */
	public Adapter createHorizontal_AmplifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Motor <em>Motor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Motor
	 * @generated
	 */
	public Adapter createMotorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Ground <em>Ground</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Ground
	 * @generated
	 */
	public Adapter createGroundAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Horizontal_Loudspeaker <em>Horizontal Loudspeaker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Horizontal_Loudspeaker
	 * @generated
	 */
	public Adapter createHorizontal_LoudspeakerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Vertical_Loudspeaker <em>Vertical Loudspeaker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Vertical_Loudspeaker
	 * @generated
	 */
	public Adapter createVertical_LoudspeakerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Cabel_Corner <em>Cabel Corner</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Cabel_Corner
	 * @generated
	 */
	public Adapter createCabel_CornerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Electric_Plan_DSL.Wire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Electric_Plan_DSL.Wire
	 * @generated
	 */
	public Adapter createWireAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Electric_Plan_DSLAdapterFactory
