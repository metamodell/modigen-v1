/**
 */
package Electric_Plan_DSL;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage
 * @generated
 */
public interface Electric_Plan_DSLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Electric_Plan_DSLFactory eINSTANCE = Electric_Plan_DSL.impl.Electric_Plan_DSLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Circuit Plan</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Circuit Plan</em>'.
	 * @generated
	 */
	Circuit_Plan createCircuit_Plan();

	/**
	 * Returns a new object of class '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component</em>'.
	 * @generated
	 */
	Component createComponent();

	/**
	 * Returns a new object of class '<em>Connection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Connection</em>'.
	 * @generated
	 */
	Connection createConnection();

	/**
	 * Returns a new object of class '<em>Lamp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lamp</em>'.
	 * @generated
	 */
	Lamp createLamp();

	/**
	 * Returns a new object of class '<em>Vertical Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Resistor</em>'.
	 * @generated
	 */
	Vertical_Resistor createVertical_Resistor();

	/**
	 * Returns a new object of class '<em>Horizontal Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Resistor</em>'.
	 * @generated
	 */
	Horizontal_Resistor createHorizontal_Resistor();

	/**
	 * Returns a new object of class '<em>Vertical Diode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Diode</em>'.
	 * @generated
	 */
	Vertical_Diode createVertical_Diode();

	/**
	 * Returns a new object of class '<em>Horizontal Diode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Diode</em>'.
	 * @generated
	 */
	Horizontal_Diode createHorizontal_Diode();

	/**
	 * Returns a new object of class '<em>Horizontal Off Switch</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Off Switch</em>'.
	 * @generated
	 */
	Horizontal_Off_Switch createHorizontal_Off_Switch();

	/**
	 * Returns a new object of class '<em>Vertical Off Switch</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Off Switch</em>'.
	 * @generated
	 */
	Vertical_Off_Switch createVertical_Off_Switch();

	/**
	 * Returns a new object of class '<em>Horizontal On Switch</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal On Switch</em>'.
	 * @generated
	 */
	Horizontal_On_Switch createHorizontal_On_Switch();

	/**
	 * Returns a new object of class '<em>Vertical On Switch</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical On Switch</em>'.
	 * @generated
	 */
	Vertical_On_Switch createVertical_On_Switch();

	/**
	 * Returns a new object of class '<em>Horizontal Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Source</em>'.
	 * @generated
	 */
	Horizontal_Source createHorizontal_Source();

	/**
	 * Returns a new object of class '<em>Vertical Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Source</em>'.
	 * @generated
	 */
	Vertical_Source createVertical_Source();

	/**
	 * Returns a new object of class '<em>Vertical Transformator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Transformator</em>'.
	 * @generated
	 */
	Vertical_Transformator createVertical_Transformator();

	/**
	 * Returns a new object of class '<em>Horizontal Transformator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Transformator</em>'.
	 * @generated
	 */
	Horizontal_Transformator createHorizontal_Transformator();

	/**
	 * Returns a new object of class '<em>Vertical Transistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Transistor</em>'.
	 * @generated
	 */
	Vertical_Transistor createVertical_Transistor();

	/**
	 * Returns a new object of class '<em>Horizontal Transistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Transistor</em>'.
	 * @generated
	 */
	Horizontal_Transistor createHorizontal_Transistor();

	/**
	 * Returns a new object of class '<em>Voltmeter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Voltmeter</em>'.
	 * @generated
	 */
	Voltmeter createVoltmeter();

	/**
	 * Returns a new object of class '<em>Ammeter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ammeter</em>'.
	 * @generated
	 */
	Ammeter createAmmeter();

	/**
	 * Returns a new object of class '<em>Vertical Fuse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Fuse</em>'.
	 * @generated
	 */
	Vertical_Fuse createVertical_Fuse();

	/**
	 * Returns a new object of class '<em>Horizontal Fuse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Fuse</em>'.
	 * @generated
	 */
	Horizontal_Fuse createHorizontal_Fuse();

	/**
	 * Returns a new object of class '<em>Vertical Thermistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Thermistor</em>'.
	 * @generated
	 */
	Vertical_Thermistor createVertical_Thermistor();

	/**
	 * Returns a new object of class '<em>Horizontal Thermistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Thermistor</em>'.
	 * @generated
	 */
	Horizontal_Thermistor createHorizontal_Thermistor();

	/**
	 * Returns a new object of class '<em>Vertical Variabel Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Variabel Resistor</em>'.
	 * @generated
	 */
	Vertical_Variabel_Resistor createVertical_Variabel_Resistor();

	/**
	 * Returns a new object of class '<em>Horizontal Variabel Resistor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Variabel Resistor</em>'.
	 * @generated
	 */
	Horizontal_Variabel_Resistor createHorizontal_Variabel_Resistor();

	/**
	 * Returns a new object of class '<em>Vertical Inductor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Inductor</em>'.
	 * @generated
	 */
	Vertical_Inductor createVertical_Inductor();

	/**
	 * Returns a new object of class '<em>Horizontal Inductor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Inductor</em>'.
	 * @generated
	 */
	Horizontal_Inductor createHorizontal_Inductor();

	/**
	 * Returns a new object of class '<em>Vertical Capacitor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Capacitor</em>'.
	 * @generated
	 */
	Vertical_Capacitor createVertical_Capacitor();

	/**
	 * Returns a new object of class '<em>Horizontal Capacitor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Capacitor</em>'.
	 * @generated
	 */
	Horizontal_Capacitor createHorizontal_Capacitor();

	/**
	 * Returns a new object of class '<em>Vertical Amplifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Amplifier</em>'.
	 * @generated
	 */
	Vertical_Amplifier createVertical_Amplifier();

	/**
	 * Returns a new object of class '<em>Horizontal Amplifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Amplifier</em>'.
	 * @generated
	 */
	Horizontal_Amplifier createHorizontal_Amplifier();

	/**
	 * Returns a new object of class '<em>Motor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Motor</em>'.
	 * @generated
	 */
	Motor createMotor();

	/**
	 * Returns a new object of class '<em>Ground</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ground</em>'.
	 * @generated
	 */
	Ground createGround();

	/**
	 * Returns a new object of class '<em>Horizontal Loudspeaker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Loudspeaker</em>'.
	 * @generated
	 */
	Horizontal_Loudspeaker createHorizontal_Loudspeaker();

	/**
	 * Returns a new object of class '<em>Vertical Loudspeaker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Loudspeaker</em>'.
	 * @generated
	 */
	Vertical_Loudspeaker createVertical_Loudspeaker();

	/**
	 * Returns a new object of class '<em>Cabel Corner</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cabel Corner</em>'.
	 * @generated
	 */
	Cabel_Corner createCabel_Corner();

	/**
	 * Returns a new object of class '<em>Wire</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wire</em>'.
	 * @generated
	 */
	Wire createWire();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Electric_Plan_DSLPackage getElectric_Plan_DSLPackage();

} //Electric_Plan_DSLFactory
