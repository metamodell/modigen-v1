/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Resistor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Resistor()
 * @model
 * @generated
 */
public interface Vertical_Resistor extends Component {
} // Vertical_Resistor
