/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Off Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Off_Switch()
 * @model
 * @generated
 */
public interface Vertical_Off_Switch extends Component {
} // Vertical_Off_Switch
