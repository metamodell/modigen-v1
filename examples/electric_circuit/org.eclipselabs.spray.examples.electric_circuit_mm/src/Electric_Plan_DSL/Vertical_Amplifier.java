/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Amplifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Amplifier()
 * @model
 * @generated
 */
public interface Vertical_Amplifier extends Component {
} // Vertical_Amplifier
