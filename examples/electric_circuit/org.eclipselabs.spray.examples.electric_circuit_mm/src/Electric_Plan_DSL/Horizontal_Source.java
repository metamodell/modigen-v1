/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Source</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getHorizontal_Source()
 * @model
 * @generated
 */
public interface Horizontal_Source extends Component {
} // Horizontal_Source
