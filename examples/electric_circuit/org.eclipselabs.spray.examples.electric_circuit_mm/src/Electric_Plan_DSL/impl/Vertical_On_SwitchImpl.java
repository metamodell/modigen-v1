/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import Electric_Plan_DSL.Vertical_On_Switch;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vertical On Switch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Vertical_On_SwitchImpl extends ComponentImpl implements Vertical_On_Switch {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Vertical_On_SwitchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.VERTICAL_ON_SWITCH;
	}

} //Vertical_On_SwitchImpl
