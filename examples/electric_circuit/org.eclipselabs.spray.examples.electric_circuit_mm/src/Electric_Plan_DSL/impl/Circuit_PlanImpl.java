/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Circuit_Plan;
import Electric_Plan_DSL.Electric_Plan_DSLPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Circuit Plan</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Electric_Plan_DSL.impl.Circuit_PlanImpl#getCircuit_name <em>Circuit name</em>}</li>
 *   <li>{@link Electric_Plan_DSL.impl.Circuit_PlanImpl#getCircuit_description <em>Circuit description</em>}</li>
 *   <li>{@link Electric_Plan_DSL.impl.Circuit_PlanImpl#getCircuit_referenz <em>Circuit referenz</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Circuit_PlanImpl extends MinimalEObjectImpl.Container implements Circuit_Plan {
	/**
	 * The default value of the '{@link #getCircuit_name() <em>Circuit name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCircuit_name()
	 * @generated
	 * @ordered
	 */
	protected static final String CIRCUIT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCircuit_name() <em>Circuit name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCircuit_name()
	 * @generated
	 * @ordered
	 */
	protected String circuit_name = CIRCUIT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCircuit_description() <em>Circuit description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCircuit_description()
	 * @generated
	 * @ordered
	 */
	protected static final String CIRCUIT_DESCRIPTION_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getCircuit_description() <em>Circuit description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCircuit_description()
	 * @generated
	 * @ordered
	 */
	protected String circuit_description = CIRCUIT_DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCircuit_referenz() <em>Circuit referenz</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCircuit_referenz()
	 * @generated
	 * @ordered
	 */
	protected EList<Circuit_Plan> circuit_referenz;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Circuit_PlanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.CIRCUIT_PLAN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCircuit_name() {
		return circuit_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCircuit_name(String newCircuit_name) {
		String oldCircuit_name = circuit_name;
		circuit_name = newCircuit_name;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_NAME, oldCircuit_name, circuit_name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCircuit_description() {
		return circuit_description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCircuit_description(String newCircuit_description) {
		String oldCircuit_description = circuit_description;
		circuit_description = newCircuit_description;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_DESCRIPTION, oldCircuit_description, circuit_description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Circuit_Plan> getCircuit_referenz() {
		if (circuit_referenz == null) {
			circuit_referenz = new EObjectContainmentEList<Circuit_Plan>(Circuit_Plan.class, this, Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_REFERENZ);
		}
		return circuit_referenz;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_REFERENZ:
				return ((InternalEList<?>)getCircuit_referenz()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_NAME:
				return getCircuit_name();
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_DESCRIPTION:
				return getCircuit_description();
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_REFERENZ:
				return getCircuit_referenz();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_NAME:
				setCircuit_name((String)newValue);
				return;
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_DESCRIPTION:
				setCircuit_description((String)newValue);
				return;
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_REFERENZ:
				getCircuit_referenz().clear();
				getCircuit_referenz().addAll((Collection<? extends Circuit_Plan>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_NAME:
				setCircuit_name(CIRCUIT_NAME_EDEFAULT);
				return;
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_DESCRIPTION:
				setCircuit_description(CIRCUIT_DESCRIPTION_EDEFAULT);
				return;
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_REFERENZ:
				getCircuit_referenz().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_NAME:
				return CIRCUIT_NAME_EDEFAULT == null ? circuit_name != null : !CIRCUIT_NAME_EDEFAULT.equals(circuit_name);
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_DESCRIPTION:
				return CIRCUIT_DESCRIPTION_EDEFAULT == null ? circuit_description != null : !CIRCUIT_DESCRIPTION_EDEFAULT.equals(circuit_description);
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN__CIRCUIT_REFERENZ:
				return circuit_referenz != null && !circuit_referenz.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (circuit_name: ");
		result.append(circuit_name);
		result.append(", circuit_description: ");
		result.append(circuit_description);
		result.append(')');
		return result.toString();
	}

} //Circuit_PlanImpl
