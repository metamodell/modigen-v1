/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import Electric_Plan_DSL.Vertical_Transistor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vertical Transistor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Vertical_TransistorImpl extends ComponentImpl implements Vertical_Transistor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Vertical_TransistorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.VERTICAL_TRANSISTOR;
	}

} //Vertical_TransistorImpl
