/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Component;
import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import Electric_Plan_DSL.Wire;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Wire</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Electric_Plan_DSL.impl.WireImpl#getFromComponent <em>From Component</em>}</li>
 *   <li>{@link Electric_Plan_DSL.impl.WireImpl#getToComponent <em>To Component</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class WireImpl extends ConnectionImpl implements Wire {
	/**
	 * The cached value of the '{@link #getFromComponent() <em>From Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromComponent()
	 * @generated
	 * @ordered
	 */
	protected Component fromComponent;

	/**
	 * The cached value of the '{@link #getToComponent() <em>To Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToComponent()
	 * @generated
	 * @ordered
	 */
	protected Component toComponent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WireImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.WIRE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getFromComponent() {
		if (fromComponent != null && fromComponent.eIsProxy()) {
			InternalEObject oldFromComponent = (InternalEObject)fromComponent;
			fromComponent = (Component)eResolveProxy(oldFromComponent);
			if (fromComponent != oldFromComponent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Electric_Plan_DSLPackage.WIRE__FROM_COMPONENT, oldFromComponent, fromComponent));
			}
		}
		return fromComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component basicGetFromComponent() {
		return fromComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromComponent(Component newFromComponent) {
		Component oldFromComponent = fromComponent;
		fromComponent = newFromComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Electric_Plan_DSLPackage.WIRE__FROM_COMPONENT, oldFromComponent, fromComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getToComponent() {
		if (toComponent != null && toComponent.eIsProxy()) {
			InternalEObject oldToComponent = (InternalEObject)toComponent;
			toComponent = (Component)eResolveProxy(oldToComponent);
			if (toComponent != oldToComponent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Electric_Plan_DSLPackage.WIRE__TO_COMPONENT, oldToComponent, toComponent));
			}
		}
		return toComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component basicGetToComponent() {
		return toComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToComponent(Component newToComponent) {
		Component oldToComponent = toComponent;
		toComponent = newToComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Electric_Plan_DSLPackage.WIRE__TO_COMPONENT, oldToComponent, toComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.WIRE__FROM_COMPONENT:
				if (resolve) return getFromComponent();
				return basicGetFromComponent();
			case Electric_Plan_DSLPackage.WIRE__TO_COMPONENT:
				if (resolve) return getToComponent();
				return basicGetToComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.WIRE__FROM_COMPONENT:
				setFromComponent((Component)newValue);
				return;
			case Electric_Plan_DSLPackage.WIRE__TO_COMPONENT:
				setToComponent((Component)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.WIRE__FROM_COMPONENT:
				setFromComponent((Component)null);
				return;
			case Electric_Plan_DSLPackage.WIRE__TO_COMPONENT:
				setToComponent((Component)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Electric_Plan_DSLPackage.WIRE__FROM_COMPONENT:
				return fromComponent != null;
			case Electric_Plan_DSLPackage.WIRE__TO_COMPONENT:
				return toComponent != null;
		}
		return super.eIsSet(featureID);
	}

} //WireImpl
