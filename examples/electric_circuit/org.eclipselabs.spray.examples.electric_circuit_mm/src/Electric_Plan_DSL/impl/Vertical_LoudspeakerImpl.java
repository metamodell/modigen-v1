/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import Electric_Plan_DSL.Vertical_Loudspeaker;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vertical Loudspeaker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Vertical_LoudspeakerImpl extends ComponentImpl implements Vertical_Loudspeaker {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Vertical_LoudspeakerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.VERTICAL_LOUDSPEAKER;
	}

} //Vertical_LoudspeakerImpl
