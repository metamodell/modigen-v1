/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Ammeter;
import Electric_Plan_DSL.Cabel_Corner;
import Electric_Plan_DSL.Circuit_Plan;
import Electric_Plan_DSL.Component;
import Electric_Plan_DSL.Connection;
import Electric_Plan_DSL.Electric_Plan_DSLFactory;
import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import Electric_Plan_DSL.Ground;
import Electric_Plan_DSL.Horizontal_Amplifier;
import Electric_Plan_DSL.Horizontal_Capacitor;
import Electric_Plan_DSL.Horizontal_Diode;
import Electric_Plan_DSL.Horizontal_Fuse;
import Electric_Plan_DSL.Horizontal_Inductor;
import Electric_Plan_DSL.Horizontal_Loudspeaker;
import Electric_Plan_DSL.Horizontal_Off_Switch;
import Electric_Plan_DSL.Horizontal_On_Switch;
import Electric_Plan_DSL.Horizontal_Resistor;
import Electric_Plan_DSL.Horizontal_Source;
import Electric_Plan_DSL.Horizontal_Thermistor;
import Electric_Plan_DSL.Horizontal_Transformator;
import Electric_Plan_DSL.Horizontal_Transistor;
import Electric_Plan_DSL.Horizontal_Variabel_Resistor;
import Electric_Plan_DSL.Lamp;
import Electric_Plan_DSL.Motor;
import Electric_Plan_DSL.Vertical_Amplifier;
import Electric_Plan_DSL.Vertical_Capacitor;
import Electric_Plan_DSL.Vertical_Diode;
import Electric_Plan_DSL.Vertical_Fuse;
import Electric_Plan_DSL.Vertical_Inductor;
import Electric_Plan_DSL.Vertical_Loudspeaker;
import Electric_Plan_DSL.Vertical_Off_Switch;
import Electric_Plan_DSL.Vertical_On_Switch;
import Electric_Plan_DSL.Vertical_Resistor;
import Electric_Plan_DSL.Vertical_Source;
import Electric_Plan_DSL.Vertical_Thermistor;
import Electric_Plan_DSL.Vertical_Transformator;
import Electric_Plan_DSL.Vertical_Transistor;
import Electric_Plan_DSL.Vertical_Variabel_Resistor;
import Electric_Plan_DSL.Voltmeter;
import Electric_Plan_DSL.Wire;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Electric_Plan_DSLPackageImpl extends EPackageImpl implements Electric_Plan_DSLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass circuit_PlanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lampEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_ResistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_ResistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_DiodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_DiodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_Off_SwitchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_Off_SwitchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_On_SwitchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_On_SwitchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_SourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_SourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_TransformatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_TransformatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_TransistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_TransistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass voltmeterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ammeterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_FuseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_FuseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_ThermistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_ThermistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_Variabel_ResistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_Variabel_ResistorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_InductorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_InductorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_CapacitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_CapacitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_AmplifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_AmplifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass motorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass groundEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontal_LoudspeakerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vertical_LoudspeakerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cabel_CornerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass wireEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Electric_Plan_DSLPackageImpl() {
		super(eNS_URI, Electric_Plan_DSLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Electric_Plan_DSLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Electric_Plan_DSLPackage init() {
		if (isInited) return (Electric_Plan_DSLPackage)EPackage.Registry.INSTANCE.getEPackage(Electric_Plan_DSLPackage.eNS_URI);

		// Obtain or create and register package
		Electric_Plan_DSLPackageImpl theElectric_Plan_DSLPackage = (Electric_Plan_DSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Electric_Plan_DSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Electric_Plan_DSLPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theElectric_Plan_DSLPackage.createPackageContents();

		// Initialize created meta-data
		theElectric_Plan_DSLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theElectric_Plan_DSLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Electric_Plan_DSLPackage.eNS_URI, theElectric_Plan_DSLPackage);
		return theElectric_Plan_DSLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCircuit_Plan() {
		return circuit_PlanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCircuit_Plan_Circuit_name() {
		return (EAttribute)circuit_PlanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCircuit_Plan_Circuit_description() {
		return (EAttribute)circuit_PlanEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCircuit_Plan_Circuit_referenz() {
		return (EReference)circuit_PlanEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponent() {
		return componentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnection() {
		return connectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLamp() {
		return lampEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Resistor() {
		return vertical_ResistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Resistor() {
		return horizontal_ResistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Diode() {
		return vertical_DiodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Diode() {
		return horizontal_DiodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Off_Switch() {
		return horizontal_Off_SwitchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Off_Switch() {
		return vertical_Off_SwitchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_On_Switch() {
		return horizontal_On_SwitchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_On_Switch() {
		return vertical_On_SwitchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Source() {
		return horizontal_SourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Source() {
		return vertical_SourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Transformator() {
		return vertical_TransformatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Transformator() {
		return horizontal_TransformatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Transistor() {
		return vertical_TransistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Transistor() {
		return horizontal_TransistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVoltmeter() {
		return voltmeterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAmmeter() {
		return ammeterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Fuse() {
		return vertical_FuseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Fuse() {
		return horizontal_FuseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Thermistor() {
		return vertical_ThermistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Thermistor() {
		return horizontal_ThermistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Variabel_Resistor() {
		return vertical_Variabel_ResistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Variabel_Resistor() {
		return horizontal_Variabel_ResistorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Inductor() {
		return vertical_InductorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Inductor() {
		return horizontal_InductorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Capacitor() {
		return vertical_CapacitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Capacitor() {
		return horizontal_CapacitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Amplifier() {
		return vertical_AmplifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Amplifier() {
		return horizontal_AmplifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMotor() {
		return motorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGround() {
		return groundEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontal_Loudspeaker() {
		return horizontal_LoudspeakerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVertical_Loudspeaker() {
		return vertical_LoudspeakerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCabel_Corner() {
		return cabel_CornerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWire() {
		return wireEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWire_FromComponent() {
		return (EReference)wireEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWire_ToComponent() {
		return (EReference)wireEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Electric_Plan_DSLFactory getElectric_Plan_DSLFactory() {
		return (Electric_Plan_DSLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		circuit_PlanEClass = createEClass(CIRCUIT_PLAN);
		createEAttribute(circuit_PlanEClass, CIRCUIT_PLAN__CIRCUIT_NAME);
		createEAttribute(circuit_PlanEClass, CIRCUIT_PLAN__CIRCUIT_DESCRIPTION);
		createEReference(circuit_PlanEClass, CIRCUIT_PLAN__CIRCUIT_REFERENZ);

		componentEClass = createEClass(COMPONENT);

		connectionEClass = createEClass(CONNECTION);

		lampEClass = createEClass(LAMP);

		vertical_ResistorEClass = createEClass(VERTICAL_RESISTOR);

		horizontal_ResistorEClass = createEClass(HORIZONTAL_RESISTOR);

		vertical_DiodeEClass = createEClass(VERTICAL_DIODE);

		horizontal_DiodeEClass = createEClass(HORIZONTAL_DIODE);

		horizontal_Off_SwitchEClass = createEClass(HORIZONTAL_OFF_SWITCH);

		vertical_Off_SwitchEClass = createEClass(VERTICAL_OFF_SWITCH);

		horizontal_On_SwitchEClass = createEClass(HORIZONTAL_ON_SWITCH);

		vertical_On_SwitchEClass = createEClass(VERTICAL_ON_SWITCH);

		horizontal_SourceEClass = createEClass(HORIZONTAL_SOURCE);

		vertical_SourceEClass = createEClass(VERTICAL_SOURCE);

		vertical_TransformatorEClass = createEClass(VERTICAL_TRANSFORMATOR);

		horizontal_TransformatorEClass = createEClass(HORIZONTAL_TRANSFORMATOR);

		vertical_TransistorEClass = createEClass(VERTICAL_TRANSISTOR);

		horizontal_TransistorEClass = createEClass(HORIZONTAL_TRANSISTOR);

		voltmeterEClass = createEClass(VOLTMETER);

		ammeterEClass = createEClass(AMMETER);

		vertical_FuseEClass = createEClass(VERTICAL_FUSE);

		horizontal_FuseEClass = createEClass(HORIZONTAL_FUSE);

		vertical_ThermistorEClass = createEClass(VERTICAL_THERMISTOR);

		horizontal_ThermistorEClass = createEClass(HORIZONTAL_THERMISTOR);

		vertical_Variabel_ResistorEClass = createEClass(VERTICAL_VARIABEL_RESISTOR);

		horizontal_Variabel_ResistorEClass = createEClass(HORIZONTAL_VARIABEL_RESISTOR);

		vertical_InductorEClass = createEClass(VERTICAL_INDUCTOR);

		horizontal_InductorEClass = createEClass(HORIZONTAL_INDUCTOR);

		vertical_CapacitorEClass = createEClass(VERTICAL_CAPACITOR);

		horizontal_CapacitorEClass = createEClass(HORIZONTAL_CAPACITOR);

		vertical_AmplifierEClass = createEClass(VERTICAL_AMPLIFIER);

		horizontal_AmplifierEClass = createEClass(HORIZONTAL_AMPLIFIER);

		motorEClass = createEClass(MOTOR);

		groundEClass = createEClass(GROUND);

		horizontal_LoudspeakerEClass = createEClass(HORIZONTAL_LOUDSPEAKER);

		vertical_LoudspeakerEClass = createEClass(VERTICAL_LOUDSPEAKER);

		cabel_CornerEClass = createEClass(CABEL_CORNER);

		wireEClass = createEClass(WIRE);
		createEReference(wireEClass, WIRE__FROM_COMPONENT);
		createEReference(wireEClass, WIRE__TO_COMPONENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		componentEClass.getESuperTypes().add(this.getCircuit_Plan());
		connectionEClass.getESuperTypes().add(this.getCircuit_Plan());
		lampEClass.getESuperTypes().add(this.getComponent());
		vertical_ResistorEClass.getESuperTypes().add(this.getComponent());
		horizontal_ResistorEClass.getESuperTypes().add(this.getComponent());
		vertical_DiodeEClass.getESuperTypes().add(this.getComponent());
		horizontal_DiodeEClass.getESuperTypes().add(this.getComponent());
		horizontal_Off_SwitchEClass.getESuperTypes().add(this.getComponent());
		vertical_Off_SwitchEClass.getESuperTypes().add(this.getComponent());
		horizontal_On_SwitchEClass.getESuperTypes().add(this.getComponent());
		vertical_On_SwitchEClass.getESuperTypes().add(this.getComponent());
		horizontal_SourceEClass.getESuperTypes().add(this.getComponent());
		vertical_SourceEClass.getESuperTypes().add(this.getComponent());
		vertical_TransformatorEClass.getESuperTypes().add(this.getComponent());
		horizontal_TransformatorEClass.getESuperTypes().add(this.getComponent());
		vertical_TransistorEClass.getESuperTypes().add(this.getComponent());
		horizontal_TransistorEClass.getESuperTypes().add(this.getComponent());
		voltmeterEClass.getESuperTypes().add(this.getComponent());
		ammeterEClass.getESuperTypes().add(this.getComponent());
		vertical_FuseEClass.getESuperTypes().add(this.getComponent());
		horizontal_FuseEClass.getESuperTypes().add(this.getComponent());
		vertical_ThermistorEClass.getESuperTypes().add(this.getComponent());
		horizontal_ThermistorEClass.getESuperTypes().add(this.getComponent());
		vertical_Variabel_ResistorEClass.getESuperTypes().add(this.getComponent());
		horizontal_Variabel_ResistorEClass.getESuperTypes().add(this.getComponent());
		vertical_InductorEClass.getESuperTypes().add(this.getComponent());
		horizontal_InductorEClass.getESuperTypes().add(this.getComponent());
		vertical_CapacitorEClass.getESuperTypes().add(this.getComponent());
		horizontal_CapacitorEClass.getESuperTypes().add(this.getComponent());
		vertical_AmplifierEClass.getESuperTypes().add(this.getComponent());
		horizontal_AmplifierEClass.getESuperTypes().add(this.getComponent());
		motorEClass.getESuperTypes().add(this.getComponent());
		groundEClass.getESuperTypes().add(this.getComponent());
		horizontal_LoudspeakerEClass.getESuperTypes().add(this.getComponent());
		vertical_LoudspeakerEClass.getESuperTypes().add(this.getComponent());
		cabel_CornerEClass.getESuperTypes().add(this.getComponent());
		wireEClass.getESuperTypes().add(this.getConnection());

		// Initialize classes, features, and operations; add parameters
		initEClass(circuit_PlanEClass, Circuit_Plan.class, "Circuit_Plan", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCircuit_Plan_Circuit_name(), ecorePackage.getEString(), "circuit_name", null, 0, 1, Circuit_Plan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCircuit_Plan_Circuit_description(), ecorePackage.getEString(), "circuit_description", "", 0, 1, Circuit_Plan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCircuit_Plan_Circuit_referenz(), this.getCircuit_Plan(), null, "circuit_referenz", null, 0, -1, Circuit_Plan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentEClass, Component.class, "Component", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(connectionEClass, Connection.class, "Connection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(lampEClass, Lamp.class, "Lamp", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_ResistorEClass, Vertical_Resistor.class, "Vertical_Resistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_ResistorEClass, Horizontal_Resistor.class, "Horizontal_Resistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_DiodeEClass, Vertical_Diode.class, "Vertical_Diode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_DiodeEClass, Horizontal_Diode.class, "Horizontal_Diode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_Off_SwitchEClass, Horizontal_Off_Switch.class, "Horizontal_Off_Switch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_Off_SwitchEClass, Vertical_Off_Switch.class, "Vertical_Off_Switch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_On_SwitchEClass, Horizontal_On_Switch.class, "Horizontal_On_Switch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_On_SwitchEClass, Vertical_On_Switch.class, "Vertical_On_Switch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_SourceEClass, Horizontal_Source.class, "Horizontal_Source", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_SourceEClass, Vertical_Source.class, "Vertical_Source", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_TransformatorEClass, Vertical_Transformator.class, "Vertical_Transformator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_TransformatorEClass, Horizontal_Transformator.class, "Horizontal_Transformator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_TransistorEClass, Vertical_Transistor.class, "Vertical_Transistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_TransistorEClass, Horizontal_Transistor.class, "Horizontal_Transistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(voltmeterEClass, Voltmeter.class, "Voltmeter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ammeterEClass, Ammeter.class, "Ammeter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_FuseEClass, Vertical_Fuse.class, "Vertical_Fuse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_FuseEClass, Horizontal_Fuse.class, "Horizontal_Fuse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_ThermistorEClass, Vertical_Thermistor.class, "Vertical_Thermistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_ThermistorEClass, Horizontal_Thermistor.class, "Horizontal_Thermistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_Variabel_ResistorEClass, Vertical_Variabel_Resistor.class, "Vertical_Variabel_Resistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_Variabel_ResistorEClass, Horizontal_Variabel_Resistor.class, "Horizontal_Variabel_Resistor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_InductorEClass, Vertical_Inductor.class, "Vertical_Inductor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_InductorEClass, Horizontal_Inductor.class, "Horizontal_Inductor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_CapacitorEClass, Vertical_Capacitor.class, "Vertical_Capacitor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_CapacitorEClass, Horizontal_Capacitor.class, "Horizontal_Capacitor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_AmplifierEClass, Vertical_Amplifier.class, "Vertical_Amplifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_AmplifierEClass, Horizontal_Amplifier.class, "Horizontal_Amplifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(motorEClass, Motor.class, "Motor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(groundEClass, Ground.class, "Ground", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontal_LoudspeakerEClass, Horizontal_Loudspeaker.class, "Horizontal_Loudspeaker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vertical_LoudspeakerEClass, Vertical_Loudspeaker.class, "Vertical_Loudspeaker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cabel_CornerEClass, Cabel_Corner.class, "Cabel_Corner", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(wireEClass, Wire.class, "Wire", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWire_FromComponent(), this.getComponent(), null, "fromComponent", null, 0, 1, Wire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWire_ToComponent(), this.getComponent(), null, "toComponent", null, 0, 1, Wire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Electric_Plan_DSLPackageImpl
