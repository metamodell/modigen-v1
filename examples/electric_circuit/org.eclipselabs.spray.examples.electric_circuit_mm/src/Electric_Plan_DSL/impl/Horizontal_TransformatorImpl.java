/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import Electric_Plan_DSL.Horizontal_Transformator;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Horizontal Transformator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Horizontal_TransformatorImpl extends ComponentImpl implements Horizontal_Transformator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Horizontal_TransformatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.HORIZONTAL_TRANSFORMATOR;
	}

} //Horizontal_TransformatorImpl
