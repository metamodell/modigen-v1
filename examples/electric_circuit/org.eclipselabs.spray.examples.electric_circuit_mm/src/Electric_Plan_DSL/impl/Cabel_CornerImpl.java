/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Cabel_Corner;
import Electric_Plan_DSL.Electric_Plan_DSLPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cabel Corner</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Cabel_CornerImpl extends ComponentImpl implements Cabel_Corner {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Cabel_CornerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.CABEL_CORNER;
	}

} //Cabel_CornerImpl
