/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Electric_Plan_DSLFactoryImpl extends EFactoryImpl implements Electric_Plan_DSLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Electric_Plan_DSLFactory init() {
		try {
			Electric_Plan_DSLFactory theElectric_Plan_DSLFactory = (Electric_Plan_DSLFactory)EPackage.Registry.INSTANCE.getEFactory(Electric_Plan_DSLPackage.eNS_URI);
			if (theElectric_Plan_DSLFactory != null) {
				return theElectric_Plan_DSLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Electric_Plan_DSLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Electric_Plan_DSLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Electric_Plan_DSLPackage.CIRCUIT_PLAN: return createCircuit_Plan();
			case Electric_Plan_DSLPackage.COMPONENT: return createComponent();
			case Electric_Plan_DSLPackage.CONNECTION: return createConnection();
			case Electric_Plan_DSLPackage.LAMP: return createLamp();
			case Electric_Plan_DSLPackage.VERTICAL_RESISTOR: return createVertical_Resistor();
			case Electric_Plan_DSLPackage.HORIZONTAL_RESISTOR: return createHorizontal_Resistor();
			case Electric_Plan_DSLPackage.VERTICAL_DIODE: return createVertical_Diode();
			case Electric_Plan_DSLPackage.HORIZONTAL_DIODE: return createHorizontal_Diode();
			case Electric_Plan_DSLPackage.HORIZONTAL_OFF_SWITCH: return createHorizontal_Off_Switch();
			case Electric_Plan_DSLPackage.VERTICAL_OFF_SWITCH: return createVertical_Off_Switch();
			case Electric_Plan_DSLPackage.HORIZONTAL_ON_SWITCH: return createHorizontal_On_Switch();
			case Electric_Plan_DSLPackage.VERTICAL_ON_SWITCH: return createVertical_On_Switch();
			case Electric_Plan_DSLPackage.HORIZONTAL_SOURCE: return createHorizontal_Source();
			case Electric_Plan_DSLPackage.VERTICAL_SOURCE: return createVertical_Source();
			case Electric_Plan_DSLPackage.VERTICAL_TRANSFORMATOR: return createVertical_Transformator();
			case Electric_Plan_DSLPackage.HORIZONTAL_TRANSFORMATOR: return createHorizontal_Transformator();
			case Electric_Plan_DSLPackage.VERTICAL_TRANSISTOR: return createVertical_Transistor();
			case Electric_Plan_DSLPackage.HORIZONTAL_TRANSISTOR: return createHorizontal_Transistor();
			case Electric_Plan_DSLPackage.VOLTMETER: return createVoltmeter();
			case Electric_Plan_DSLPackage.AMMETER: return createAmmeter();
			case Electric_Plan_DSLPackage.VERTICAL_FUSE: return createVertical_Fuse();
			case Electric_Plan_DSLPackage.HORIZONTAL_FUSE: return createHorizontal_Fuse();
			case Electric_Plan_DSLPackage.VERTICAL_THERMISTOR: return createVertical_Thermistor();
			case Electric_Plan_DSLPackage.HORIZONTAL_THERMISTOR: return createHorizontal_Thermistor();
			case Electric_Plan_DSLPackage.VERTICAL_VARIABEL_RESISTOR: return createVertical_Variabel_Resistor();
			case Electric_Plan_DSLPackage.HORIZONTAL_VARIABEL_RESISTOR: return createHorizontal_Variabel_Resistor();
			case Electric_Plan_DSLPackage.VERTICAL_INDUCTOR: return createVertical_Inductor();
			case Electric_Plan_DSLPackage.HORIZONTAL_INDUCTOR: return createHorizontal_Inductor();
			case Electric_Plan_DSLPackage.VERTICAL_CAPACITOR: return createVertical_Capacitor();
			case Electric_Plan_DSLPackage.HORIZONTAL_CAPACITOR: return createHorizontal_Capacitor();
			case Electric_Plan_DSLPackage.VERTICAL_AMPLIFIER: return createVertical_Amplifier();
			case Electric_Plan_DSLPackage.HORIZONTAL_AMPLIFIER: return createHorizontal_Amplifier();
			case Electric_Plan_DSLPackage.MOTOR: return createMotor();
			case Electric_Plan_DSLPackage.GROUND: return createGround();
			case Electric_Plan_DSLPackage.HORIZONTAL_LOUDSPEAKER: return createHorizontal_Loudspeaker();
			case Electric_Plan_DSLPackage.VERTICAL_LOUDSPEAKER: return createVertical_Loudspeaker();
			case Electric_Plan_DSLPackage.CABEL_CORNER: return createCabel_Corner();
			case Electric_Plan_DSLPackage.WIRE: return createWire();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Circuit_Plan createCircuit_Plan() {
		Circuit_PlanImpl circuit_Plan = new Circuit_PlanImpl();
		return circuit_Plan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component createComponent() {
		ComponentImpl component = new ComponentImpl();
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Connection createConnection() {
		ConnectionImpl connection = new ConnectionImpl();
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lamp createLamp() {
		LampImpl lamp = new LampImpl();
		return lamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Resistor createVertical_Resistor() {
		Vertical_ResistorImpl vertical_Resistor = new Vertical_ResistorImpl();
		return vertical_Resistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Resistor createHorizontal_Resistor() {
		Horizontal_ResistorImpl horizontal_Resistor = new Horizontal_ResistorImpl();
		return horizontal_Resistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Diode createVertical_Diode() {
		Vertical_DiodeImpl vertical_Diode = new Vertical_DiodeImpl();
		return vertical_Diode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Diode createHorizontal_Diode() {
		Horizontal_DiodeImpl horizontal_Diode = new Horizontal_DiodeImpl();
		return horizontal_Diode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Off_Switch createHorizontal_Off_Switch() {
		Horizontal_Off_SwitchImpl horizontal_Off_Switch = new Horizontal_Off_SwitchImpl();
		return horizontal_Off_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Off_Switch createVertical_Off_Switch() {
		Vertical_Off_SwitchImpl vertical_Off_Switch = new Vertical_Off_SwitchImpl();
		return vertical_Off_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_On_Switch createHorizontal_On_Switch() {
		Horizontal_On_SwitchImpl horizontal_On_Switch = new Horizontal_On_SwitchImpl();
		return horizontal_On_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_On_Switch createVertical_On_Switch() {
		Vertical_On_SwitchImpl vertical_On_Switch = new Vertical_On_SwitchImpl();
		return vertical_On_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Source createHorizontal_Source() {
		Horizontal_SourceImpl horizontal_Source = new Horizontal_SourceImpl();
		return horizontal_Source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Source createVertical_Source() {
		Vertical_SourceImpl vertical_Source = new Vertical_SourceImpl();
		return vertical_Source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Transformator createVertical_Transformator() {
		Vertical_TransformatorImpl vertical_Transformator = new Vertical_TransformatorImpl();
		return vertical_Transformator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Transformator createHorizontal_Transformator() {
		Horizontal_TransformatorImpl horizontal_Transformator = new Horizontal_TransformatorImpl();
		return horizontal_Transformator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Transistor createVertical_Transistor() {
		Vertical_TransistorImpl vertical_Transistor = new Vertical_TransistorImpl();
		return vertical_Transistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Transistor createHorizontal_Transistor() {
		Horizontal_TransistorImpl horizontal_Transistor = new Horizontal_TransistorImpl();
		return horizontal_Transistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Voltmeter createVoltmeter() {
		VoltmeterImpl voltmeter = new VoltmeterImpl();
		return voltmeter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ammeter createAmmeter() {
		AmmeterImpl ammeter = new AmmeterImpl();
		return ammeter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Fuse createVertical_Fuse() {
		Vertical_FuseImpl vertical_Fuse = new Vertical_FuseImpl();
		return vertical_Fuse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Fuse createHorizontal_Fuse() {
		Horizontal_FuseImpl horizontal_Fuse = new Horizontal_FuseImpl();
		return horizontal_Fuse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Thermistor createVertical_Thermistor() {
		Vertical_ThermistorImpl vertical_Thermistor = new Vertical_ThermistorImpl();
		return vertical_Thermistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Thermistor createHorizontal_Thermistor() {
		Horizontal_ThermistorImpl horizontal_Thermistor = new Horizontal_ThermistorImpl();
		return horizontal_Thermistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Variabel_Resistor createVertical_Variabel_Resistor() {
		Vertical_Variabel_ResistorImpl vertical_Variabel_Resistor = new Vertical_Variabel_ResistorImpl();
		return vertical_Variabel_Resistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Variabel_Resistor createHorizontal_Variabel_Resistor() {
		Horizontal_Variabel_ResistorImpl horizontal_Variabel_Resistor = new Horizontal_Variabel_ResistorImpl();
		return horizontal_Variabel_Resistor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Inductor createVertical_Inductor() {
		Vertical_InductorImpl vertical_Inductor = new Vertical_InductorImpl();
		return vertical_Inductor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Inductor createHorizontal_Inductor() {
		Horizontal_InductorImpl horizontal_Inductor = new Horizontal_InductorImpl();
		return horizontal_Inductor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Capacitor createVertical_Capacitor() {
		Vertical_CapacitorImpl vertical_Capacitor = new Vertical_CapacitorImpl();
		return vertical_Capacitor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Capacitor createHorizontal_Capacitor() {
		Horizontal_CapacitorImpl horizontal_Capacitor = new Horizontal_CapacitorImpl();
		return horizontal_Capacitor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Amplifier createVertical_Amplifier() {
		Vertical_AmplifierImpl vertical_Amplifier = new Vertical_AmplifierImpl();
		return vertical_Amplifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Amplifier createHorizontal_Amplifier() {
		Horizontal_AmplifierImpl horizontal_Amplifier = new Horizontal_AmplifierImpl();
		return horizontal_Amplifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Motor createMotor() {
		MotorImpl motor = new MotorImpl();
		return motor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ground createGround() {
		GroundImpl ground = new GroundImpl();
		return ground;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Horizontal_Loudspeaker createHorizontal_Loudspeaker() {
		Horizontal_LoudspeakerImpl horizontal_Loudspeaker = new Horizontal_LoudspeakerImpl();
		return horizontal_Loudspeaker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vertical_Loudspeaker createVertical_Loudspeaker() {
		Vertical_LoudspeakerImpl vertical_Loudspeaker = new Vertical_LoudspeakerImpl();
		return vertical_Loudspeaker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cabel_Corner createCabel_Corner() {
		Cabel_CornerImpl cabel_Corner = new Cabel_CornerImpl();
		return cabel_Corner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Wire createWire() {
		WireImpl wire = new WireImpl();
		return wire;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Electric_Plan_DSLPackage getElectric_Plan_DSLPackage() {
		return (Electric_Plan_DSLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Electric_Plan_DSLPackage getPackage() {
		return Electric_Plan_DSLPackage.eINSTANCE;
	}

} //Electric_Plan_DSLFactoryImpl
