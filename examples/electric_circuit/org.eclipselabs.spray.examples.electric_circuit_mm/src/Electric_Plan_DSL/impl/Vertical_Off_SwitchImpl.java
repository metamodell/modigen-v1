/**
 */
package Electric_Plan_DSL.impl;

import Electric_Plan_DSL.Electric_Plan_DSLPackage;
import Electric_Plan_DSL.Vertical_Off_Switch;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vertical Off Switch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class Vertical_Off_SwitchImpl extends ComponentImpl implements Vertical_Off_Switch {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Vertical_Off_SwitchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Electric_Plan_DSLPackage.Literals.VERTICAL_OFF_SWITCH;
	}

} //Vertical_Off_SwitchImpl
