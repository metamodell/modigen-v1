/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Transistor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getHorizontal_Transistor()
 * @model
 * @generated
 */
public interface Horizontal_Transistor extends Component {
} // Horizontal_Transistor
