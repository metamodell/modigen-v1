/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Source</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Source()
 * @model
 * @generated
 */
public interface Vertical_Source extends Component {
} // Vertical_Source
