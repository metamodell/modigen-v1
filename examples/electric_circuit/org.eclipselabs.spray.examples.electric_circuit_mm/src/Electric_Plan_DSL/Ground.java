/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ground</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getGround()
 * @model
 * @generated
 */
public interface Ground extends Component {
} // Ground
