/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Amplifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getHorizontal_Amplifier()
 * @model
 * @generated
 */
public interface Horizontal_Amplifier extends Component {
} // Horizontal_Amplifier
