/**
 */
package Electric_Plan_DSL;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Electric_Plan_DSL.Electric_Plan_DSLFactory
 * @model kind="package"
 * @generated
 */
public interface Electric_Plan_DSLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Electric_Plan_DSL";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://spray.eclipselabs.org/examples/electric_plan";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Electric_Plan";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Electric_Plan_DSLPackage eINSTANCE = Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl.init();

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Circuit_PlanImpl <em>Circuit Plan</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Circuit_PlanImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getCircuit_Plan()
	 * @generated
	 */
	int CIRCUIT_PLAN = 0;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCUIT_PLAN__CIRCUIT_NAME = 0;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCUIT_PLAN__CIRCUIT_DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCUIT_PLAN__CIRCUIT_REFERENZ = 2;

	/**
	 * The number of structural features of the '<em>Circuit Plan</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCUIT_PLAN_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Circuit Plan</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCUIT_PLAN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.ComponentImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CIRCUIT_NAME = CIRCUIT_PLAN__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CIRCUIT_DESCRIPTION = CIRCUIT_PLAN__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CIRCUIT_REFERENZ = CIRCUIT_PLAN__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = CIRCUIT_PLAN_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = CIRCUIT_PLAN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.ConnectionImpl <em>Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.ConnectionImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getConnection()
	 * @generated
	 */
	int CONNECTION = 2;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__CIRCUIT_NAME = CIRCUIT_PLAN__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__CIRCUIT_DESCRIPTION = CIRCUIT_PLAN__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__CIRCUIT_REFERENZ = CIRCUIT_PLAN__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_FEATURE_COUNT = CIRCUIT_PLAN_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_OPERATION_COUNT = CIRCUIT_PLAN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.LampImpl <em>Lamp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.LampImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getLamp()
	 * @generated
	 */
	int LAMP = 3;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMP__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMP__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMP__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Lamp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMP_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lamp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAMP_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_ResistorImpl <em>Vertical Resistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_ResistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Resistor()
	 * @generated
	 */
	int VERTICAL_RESISTOR = 4;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_RESISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_RESISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_RESISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_RESISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_RESISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_ResistorImpl <em>Horizontal Resistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_ResistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Resistor()
	 * @generated
	 */
	int HORIZONTAL_RESISTOR = 5;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_RESISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_RESISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_RESISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_RESISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_RESISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_DiodeImpl <em>Vertical Diode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_DiodeImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Diode()
	 * @generated
	 */
	int VERTICAL_DIODE = 6;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_DIODE__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_DIODE__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_DIODE__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Diode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_DIODE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Diode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_DIODE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_DiodeImpl <em>Horizontal Diode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_DiodeImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Diode()
	 * @generated
	 */
	int HORIZONTAL_DIODE = 7;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_DIODE__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_DIODE__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_DIODE__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Diode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_DIODE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Diode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_DIODE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_Off_SwitchImpl <em>Horizontal Off Switch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_Off_SwitchImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Off_Switch()
	 * @generated
	 */
	int HORIZONTAL_OFF_SWITCH = 8;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_OFF_SWITCH__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_OFF_SWITCH__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_OFF_SWITCH__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Off Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_OFF_SWITCH_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Off Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_OFF_SWITCH_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_Off_SwitchImpl <em>Vertical Off Switch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_Off_SwitchImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Off_Switch()
	 * @generated
	 */
	int VERTICAL_OFF_SWITCH = 9;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_OFF_SWITCH__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_OFF_SWITCH__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_OFF_SWITCH__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Off Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_OFF_SWITCH_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Off Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_OFF_SWITCH_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_On_SwitchImpl <em>Horizontal On Switch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_On_SwitchImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_On_Switch()
	 * @generated
	 */
	int HORIZONTAL_ON_SWITCH = 10;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_ON_SWITCH__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_ON_SWITCH__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_ON_SWITCH__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal On Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_ON_SWITCH_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal On Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_ON_SWITCH_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_On_SwitchImpl <em>Vertical On Switch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_On_SwitchImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_On_Switch()
	 * @generated
	 */
	int VERTICAL_ON_SWITCH = 11;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_ON_SWITCH__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_ON_SWITCH__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_ON_SWITCH__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical On Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_ON_SWITCH_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical On Switch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_ON_SWITCH_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_SourceImpl <em>Horizontal Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_SourceImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Source()
	 * @generated
	 */
	int HORIZONTAL_SOURCE = 12;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SOURCE__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SOURCE__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SOURCE__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SOURCE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SOURCE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_SourceImpl <em>Vertical Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_SourceImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Source()
	 * @generated
	 */
	int VERTICAL_SOURCE = 13;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SOURCE__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SOURCE__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SOURCE__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SOURCE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SOURCE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_TransformatorImpl <em>Vertical Transformator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_TransformatorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Transformator()
	 * @generated
	 */
	int VERTICAL_TRANSFORMATOR = 14;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSFORMATOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSFORMATOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSFORMATOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Transformator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSFORMATOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Transformator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSFORMATOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_TransformatorImpl <em>Horizontal Transformator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_TransformatorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Transformator()
	 * @generated
	 */
	int HORIZONTAL_TRANSFORMATOR = 15;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSFORMATOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSFORMATOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSFORMATOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Transformator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSFORMATOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Transformator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSFORMATOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_TransistorImpl <em>Vertical Transistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_TransistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Transistor()
	 * @generated
	 */
	int VERTICAL_TRANSISTOR = 16;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Transistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Transistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_TRANSISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_TransistorImpl <em>Horizontal Transistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_TransistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Transistor()
	 * @generated
	 */
	int HORIZONTAL_TRANSISTOR = 17;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Transistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Transistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_TRANSISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.VoltmeterImpl <em>Voltmeter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.VoltmeterImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVoltmeter()
	 * @generated
	 */
	int VOLTMETER = 18;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOLTMETER__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOLTMETER__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOLTMETER__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Voltmeter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOLTMETER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Voltmeter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOLTMETER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.AmmeterImpl <em>Ammeter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.AmmeterImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getAmmeter()
	 * @generated
	 */
	int AMMETER = 19;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMMETER__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMMETER__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMMETER__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Ammeter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMMETER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ammeter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AMMETER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_FuseImpl <em>Vertical Fuse</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_FuseImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Fuse()
	 * @generated
	 */
	int VERTICAL_FUSE = 20;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_FUSE__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_FUSE__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_FUSE__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Fuse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_FUSE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Fuse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_FUSE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_FuseImpl <em>Horizontal Fuse</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_FuseImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Fuse()
	 * @generated
	 */
	int HORIZONTAL_FUSE = 21;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_FUSE__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_FUSE__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_FUSE__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Fuse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_FUSE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Fuse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_FUSE_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_ThermistorImpl <em>Vertical Thermistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_ThermistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Thermistor()
	 * @generated
	 */
	int VERTICAL_THERMISTOR = 22;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_THERMISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_THERMISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_THERMISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Thermistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_THERMISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Thermistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_THERMISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_ThermistorImpl <em>Horizontal Thermistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_ThermistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Thermistor()
	 * @generated
	 */
	int HORIZONTAL_THERMISTOR = 23;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_THERMISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_THERMISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_THERMISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Thermistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_THERMISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Thermistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_THERMISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_Variabel_ResistorImpl <em>Vertical Variabel Resistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_Variabel_ResistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Variabel_Resistor()
	 * @generated
	 */
	int VERTICAL_VARIABEL_RESISTOR = 24;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_VARIABEL_RESISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_VARIABEL_RESISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_VARIABEL_RESISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Variabel Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_VARIABEL_RESISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Variabel Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_VARIABEL_RESISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_Variabel_ResistorImpl <em>Horizontal Variabel Resistor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_Variabel_ResistorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Variabel_Resistor()
	 * @generated
	 */
	int HORIZONTAL_VARIABEL_RESISTOR = 25;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_VARIABEL_RESISTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_VARIABEL_RESISTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_VARIABEL_RESISTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Variabel Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_VARIABEL_RESISTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Variabel Resistor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_VARIABEL_RESISTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_InductorImpl <em>Vertical Inductor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_InductorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Inductor()
	 * @generated
	 */
	int VERTICAL_INDUCTOR = 26;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_INDUCTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_INDUCTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_INDUCTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Inductor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_INDUCTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Inductor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_INDUCTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_InductorImpl <em>Horizontal Inductor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_InductorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Inductor()
	 * @generated
	 */
	int HORIZONTAL_INDUCTOR = 27;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_INDUCTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_INDUCTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_INDUCTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Inductor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_INDUCTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Inductor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_INDUCTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_CapacitorImpl <em>Vertical Capacitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_CapacitorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Capacitor()
	 * @generated
	 */
	int VERTICAL_CAPACITOR = 28;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_CAPACITOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_CAPACITOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_CAPACITOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Capacitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_CAPACITOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Capacitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_CAPACITOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_CapacitorImpl <em>Horizontal Capacitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_CapacitorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Capacitor()
	 * @generated
	 */
	int HORIZONTAL_CAPACITOR = 29;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_CAPACITOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_CAPACITOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_CAPACITOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Capacitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_CAPACITOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Capacitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_CAPACITOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_AmplifierImpl <em>Vertical Amplifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_AmplifierImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Amplifier()
	 * @generated
	 */
	int VERTICAL_AMPLIFIER = 30;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_AMPLIFIER__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_AMPLIFIER__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_AMPLIFIER__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Amplifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_AMPLIFIER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Amplifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_AMPLIFIER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_AmplifierImpl <em>Horizontal Amplifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_AmplifierImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Amplifier()
	 * @generated
	 */
	int HORIZONTAL_AMPLIFIER = 31;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_AMPLIFIER__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_AMPLIFIER__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_AMPLIFIER__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Amplifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_AMPLIFIER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Amplifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_AMPLIFIER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.MotorImpl <em>Motor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.MotorImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getMotor()
	 * @generated
	 */
	int MOTOR = 32;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Motor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Motor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTOR_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.GroundImpl <em>Ground</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.GroundImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getGround()
	 * @generated
	 */
	int GROUND = 33;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Ground</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ground</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUND_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Horizontal_LoudspeakerImpl <em>Horizontal Loudspeaker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Horizontal_LoudspeakerImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Loudspeaker()
	 * @generated
	 */
	int HORIZONTAL_LOUDSPEAKER = 34;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_LOUDSPEAKER__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_LOUDSPEAKER__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_LOUDSPEAKER__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Horizontal Loudspeaker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_LOUDSPEAKER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Loudspeaker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_LOUDSPEAKER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Vertical_LoudspeakerImpl <em>Vertical Loudspeaker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Vertical_LoudspeakerImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Loudspeaker()
	 * @generated
	 */
	int VERTICAL_LOUDSPEAKER = 35;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_LOUDSPEAKER__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_LOUDSPEAKER__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_LOUDSPEAKER__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Vertical Loudspeaker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_LOUDSPEAKER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Loudspeaker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_LOUDSPEAKER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.Cabel_CornerImpl <em>Cabel Corner</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.Cabel_CornerImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getCabel_Corner()
	 * @generated
	 */
	int CABEL_CORNER = 36;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CABEL_CORNER__CIRCUIT_NAME = COMPONENT__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CABEL_CORNER__CIRCUIT_DESCRIPTION = COMPONENT__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CABEL_CORNER__CIRCUIT_REFERENZ = COMPONENT__CIRCUIT_REFERENZ;

	/**
	 * The number of structural features of the '<em>Cabel Corner</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CABEL_CORNER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Cabel Corner</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CABEL_CORNER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Electric_Plan_DSL.impl.WireImpl <em>Wire</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Electric_Plan_DSL.impl.WireImpl
	 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getWire()
	 * @generated
	 */
	int WIRE = 37;

	/**
	 * The feature id for the '<em><b>Circuit name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE__CIRCUIT_NAME = CONNECTION__CIRCUIT_NAME;

	/**
	 * The feature id for the '<em><b>Circuit description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE__CIRCUIT_DESCRIPTION = CONNECTION__CIRCUIT_DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Circuit referenz</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE__CIRCUIT_REFERENZ = CONNECTION__CIRCUIT_REFERENZ;

	/**
	 * The feature id for the '<em><b>From Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE__FROM_COMPONENT = CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>To Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE__TO_COMPONENT = CONNECTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Wire</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE_FEATURE_COUNT = CONNECTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Wire</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE_OPERATION_COUNT = CONNECTION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Circuit_Plan <em>Circuit Plan</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Circuit Plan</em>'.
	 * @see Electric_Plan_DSL.Circuit_Plan
	 * @generated
	 */
	EClass getCircuit_Plan();

	/**
	 * Returns the meta object for the attribute '{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_name <em>Circuit name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Circuit name</em>'.
	 * @see Electric_Plan_DSL.Circuit_Plan#getCircuit_name()
	 * @see #getCircuit_Plan()
	 * @generated
	 */
	EAttribute getCircuit_Plan_Circuit_name();

	/**
	 * Returns the meta object for the attribute '{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_description <em>Circuit description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Circuit description</em>'.
	 * @see Electric_Plan_DSL.Circuit_Plan#getCircuit_description()
	 * @see #getCircuit_Plan()
	 * @generated
	 */
	EAttribute getCircuit_Plan_Circuit_description();

	/**
	 * Returns the meta object for the containment reference list '{@link Electric_Plan_DSL.Circuit_Plan#getCircuit_referenz <em>Circuit referenz</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Circuit referenz</em>'.
	 * @see Electric_Plan_DSL.Circuit_Plan#getCircuit_referenz()
	 * @see #getCircuit_Plan()
	 * @generated
	 */
	EReference getCircuit_Plan_Circuit_referenz();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see Electric_Plan_DSL.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Connection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection</em>'.
	 * @see Electric_Plan_DSL.Connection
	 * @generated
	 */
	EClass getConnection();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Lamp <em>Lamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lamp</em>'.
	 * @see Electric_Plan_DSL.Lamp
	 * @generated
	 */
	EClass getLamp();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Resistor <em>Vertical Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Resistor</em>'.
	 * @see Electric_Plan_DSL.Vertical_Resistor
	 * @generated
	 */
	EClass getVertical_Resistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Resistor <em>Horizontal Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Resistor</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Resistor
	 * @generated
	 */
	EClass getHorizontal_Resistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Diode <em>Vertical Diode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Diode</em>'.
	 * @see Electric_Plan_DSL.Vertical_Diode
	 * @generated
	 */
	EClass getVertical_Diode();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Diode <em>Horizontal Diode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Diode</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Diode
	 * @generated
	 */
	EClass getHorizontal_Diode();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Off_Switch <em>Horizontal Off Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Off Switch</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Off_Switch
	 * @generated
	 */
	EClass getHorizontal_Off_Switch();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Off_Switch <em>Vertical Off Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Off Switch</em>'.
	 * @see Electric_Plan_DSL.Vertical_Off_Switch
	 * @generated
	 */
	EClass getVertical_Off_Switch();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_On_Switch <em>Horizontal On Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal On Switch</em>'.
	 * @see Electric_Plan_DSL.Horizontal_On_Switch
	 * @generated
	 */
	EClass getHorizontal_On_Switch();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_On_Switch <em>Vertical On Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical On Switch</em>'.
	 * @see Electric_Plan_DSL.Vertical_On_Switch
	 * @generated
	 */
	EClass getVertical_On_Switch();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Source <em>Horizontal Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Source</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Source
	 * @generated
	 */
	EClass getHorizontal_Source();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Source <em>Vertical Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Source</em>'.
	 * @see Electric_Plan_DSL.Vertical_Source
	 * @generated
	 */
	EClass getVertical_Source();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Transformator <em>Vertical Transformator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Transformator</em>'.
	 * @see Electric_Plan_DSL.Vertical_Transformator
	 * @generated
	 */
	EClass getVertical_Transformator();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Transformator <em>Horizontal Transformator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Transformator</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Transformator
	 * @generated
	 */
	EClass getHorizontal_Transformator();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Transistor <em>Vertical Transistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Transistor</em>'.
	 * @see Electric_Plan_DSL.Vertical_Transistor
	 * @generated
	 */
	EClass getVertical_Transistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Transistor <em>Horizontal Transistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Transistor</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Transistor
	 * @generated
	 */
	EClass getHorizontal_Transistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Voltmeter <em>Voltmeter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Voltmeter</em>'.
	 * @see Electric_Plan_DSL.Voltmeter
	 * @generated
	 */
	EClass getVoltmeter();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Ammeter <em>Ammeter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ammeter</em>'.
	 * @see Electric_Plan_DSL.Ammeter
	 * @generated
	 */
	EClass getAmmeter();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Fuse <em>Vertical Fuse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Fuse</em>'.
	 * @see Electric_Plan_DSL.Vertical_Fuse
	 * @generated
	 */
	EClass getVertical_Fuse();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Fuse <em>Horizontal Fuse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Fuse</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Fuse
	 * @generated
	 */
	EClass getHorizontal_Fuse();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Thermistor <em>Vertical Thermistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Thermistor</em>'.
	 * @see Electric_Plan_DSL.Vertical_Thermistor
	 * @generated
	 */
	EClass getVertical_Thermistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Thermistor <em>Horizontal Thermistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Thermistor</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Thermistor
	 * @generated
	 */
	EClass getHorizontal_Thermistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Variabel_Resistor <em>Vertical Variabel Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Variabel Resistor</em>'.
	 * @see Electric_Plan_DSL.Vertical_Variabel_Resistor
	 * @generated
	 */
	EClass getVertical_Variabel_Resistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Variabel_Resistor <em>Horizontal Variabel Resistor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Variabel Resistor</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Variabel_Resistor
	 * @generated
	 */
	EClass getHorizontal_Variabel_Resistor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Inductor <em>Vertical Inductor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Inductor</em>'.
	 * @see Electric_Plan_DSL.Vertical_Inductor
	 * @generated
	 */
	EClass getVertical_Inductor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Inductor <em>Horizontal Inductor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Inductor</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Inductor
	 * @generated
	 */
	EClass getHorizontal_Inductor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Capacitor <em>Vertical Capacitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Capacitor</em>'.
	 * @see Electric_Plan_DSL.Vertical_Capacitor
	 * @generated
	 */
	EClass getVertical_Capacitor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Capacitor <em>Horizontal Capacitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Capacitor</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Capacitor
	 * @generated
	 */
	EClass getHorizontal_Capacitor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Amplifier <em>Vertical Amplifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Amplifier</em>'.
	 * @see Electric_Plan_DSL.Vertical_Amplifier
	 * @generated
	 */
	EClass getVertical_Amplifier();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Amplifier <em>Horizontal Amplifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Amplifier</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Amplifier
	 * @generated
	 */
	EClass getHorizontal_Amplifier();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Motor <em>Motor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Motor</em>'.
	 * @see Electric_Plan_DSL.Motor
	 * @generated
	 */
	EClass getMotor();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Ground <em>Ground</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ground</em>'.
	 * @see Electric_Plan_DSL.Ground
	 * @generated
	 */
	EClass getGround();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Horizontal_Loudspeaker <em>Horizontal Loudspeaker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Loudspeaker</em>'.
	 * @see Electric_Plan_DSL.Horizontal_Loudspeaker
	 * @generated
	 */
	EClass getHorizontal_Loudspeaker();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Vertical_Loudspeaker <em>Vertical Loudspeaker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Loudspeaker</em>'.
	 * @see Electric_Plan_DSL.Vertical_Loudspeaker
	 * @generated
	 */
	EClass getVertical_Loudspeaker();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Cabel_Corner <em>Cabel Corner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cabel Corner</em>'.
	 * @see Electric_Plan_DSL.Cabel_Corner
	 * @generated
	 */
	EClass getCabel_Corner();

	/**
	 * Returns the meta object for class '{@link Electric_Plan_DSL.Wire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wire</em>'.
	 * @see Electric_Plan_DSL.Wire
	 * @generated
	 */
	EClass getWire();

	/**
	 * Returns the meta object for the reference '{@link Electric_Plan_DSL.Wire#getFromComponent <em>From Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From Component</em>'.
	 * @see Electric_Plan_DSL.Wire#getFromComponent()
	 * @see #getWire()
	 * @generated
	 */
	EReference getWire_FromComponent();

	/**
	 * Returns the meta object for the reference '{@link Electric_Plan_DSL.Wire#getToComponent <em>To Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To Component</em>'.
	 * @see Electric_Plan_DSL.Wire#getToComponent()
	 * @see #getWire()
	 * @generated
	 */
	EReference getWire_ToComponent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Electric_Plan_DSLFactory getElectric_Plan_DSLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Circuit_PlanImpl <em>Circuit Plan</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Circuit_PlanImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getCircuit_Plan()
		 * @generated
		 */
		EClass CIRCUIT_PLAN = eINSTANCE.getCircuit_Plan();

		/**
		 * The meta object literal for the '<em><b>Circuit name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CIRCUIT_PLAN__CIRCUIT_NAME = eINSTANCE.getCircuit_Plan_Circuit_name();

		/**
		 * The meta object literal for the '<em><b>Circuit description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CIRCUIT_PLAN__CIRCUIT_DESCRIPTION = eINSTANCE.getCircuit_Plan_Circuit_description();

		/**
		 * The meta object literal for the '<em><b>Circuit referenz</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CIRCUIT_PLAN__CIRCUIT_REFERENZ = eINSTANCE.getCircuit_Plan_Circuit_referenz();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.ComponentImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.ConnectionImpl <em>Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.ConnectionImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getConnection()
		 * @generated
		 */
		EClass CONNECTION = eINSTANCE.getConnection();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.LampImpl <em>Lamp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.LampImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getLamp()
		 * @generated
		 */
		EClass LAMP = eINSTANCE.getLamp();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_ResistorImpl <em>Vertical Resistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_ResistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Resistor()
		 * @generated
		 */
		EClass VERTICAL_RESISTOR = eINSTANCE.getVertical_Resistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_ResistorImpl <em>Horizontal Resistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_ResistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Resistor()
		 * @generated
		 */
		EClass HORIZONTAL_RESISTOR = eINSTANCE.getHorizontal_Resistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_DiodeImpl <em>Vertical Diode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_DiodeImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Diode()
		 * @generated
		 */
		EClass VERTICAL_DIODE = eINSTANCE.getVertical_Diode();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_DiodeImpl <em>Horizontal Diode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_DiodeImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Diode()
		 * @generated
		 */
		EClass HORIZONTAL_DIODE = eINSTANCE.getHorizontal_Diode();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_Off_SwitchImpl <em>Horizontal Off Switch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_Off_SwitchImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Off_Switch()
		 * @generated
		 */
		EClass HORIZONTAL_OFF_SWITCH = eINSTANCE.getHorizontal_Off_Switch();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_Off_SwitchImpl <em>Vertical Off Switch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_Off_SwitchImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Off_Switch()
		 * @generated
		 */
		EClass VERTICAL_OFF_SWITCH = eINSTANCE.getVertical_Off_Switch();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_On_SwitchImpl <em>Horizontal On Switch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_On_SwitchImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_On_Switch()
		 * @generated
		 */
		EClass HORIZONTAL_ON_SWITCH = eINSTANCE.getHorizontal_On_Switch();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_On_SwitchImpl <em>Vertical On Switch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_On_SwitchImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_On_Switch()
		 * @generated
		 */
		EClass VERTICAL_ON_SWITCH = eINSTANCE.getVertical_On_Switch();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_SourceImpl <em>Horizontal Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_SourceImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Source()
		 * @generated
		 */
		EClass HORIZONTAL_SOURCE = eINSTANCE.getHorizontal_Source();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_SourceImpl <em>Vertical Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_SourceImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Source()
		 * @generated
		 */
		EClass VERTICAL_SOURCE = eINSTANCE.getVertical_Source();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_TransformatorImpl <em>Vertical Transformator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_TransformatorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Transformator()
		 * @generated
		 */
		EClass VERTICAL_TRANSFORMATOR = eINSTANCE.getVertical_Transformator();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_TransformatorImpl <em>Horizontal Transformator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_TransformatorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Transformator()
		 * @generated
		 */
		EClass HORIZONTAL_TRANSFORMATOR = eINSTANCE.getHorizontal_Transformator();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_TransistorImpl <em>Vertical Transistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_TransistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Transistor()
		 * @generated
		 */
		EClass VERTICAL_TRANSISTOR = eINSTANCE.getVertical_Transistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_TransistorImpl <em>Horizontal Transistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_TransistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Transistor()
		 * @generated
		 */
		EClass HORIZONTAL_TRANSISTOR = eINSTANCE.getHorizontal_Transistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.VoltmeterImpl <em>Voltmeter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.VoltmeterImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVoltmeter()
		 * @generated
		 */
		EClass VOLTMETER = eINSTANCE.getVoltmeter();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.AmmeterImpl <em>Ammeter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.AmmeterImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getAmmeter()
		 * @generated
		 */
		EClass AMMETER = eINSTANCE.getAmmeter();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_FuseImpl <em>Vertical Fuse</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_FuseImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Fuse()
		 * @generated
		 */
		EClass VERTICAL_FUSE = eINSTANCE.getVertical_Fuse();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_FuseImpl <em>Horizontal Fuse</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_FuseImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Fuse()
		 * @generated
		 */
		EClass HORIZONTAL_FUSE = eINSTANCE.getHorizontal_Fuse();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_ThermistorImpl <em>Vertical Thermistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_ThermistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Thermistor()
		 * @generated
		 */
		EClass VERTICAL_THERMISTOR = eINSTANCE.getVertical_Thermistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_ThermistorImpl <em>Horizontal Thermistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_ThermistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Thermistor()
		 * @generated
		 */
		EClass HORIZONTAL_THERMISTOR = eINSTANCE.getHorizontal_Thermistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_Variabel_ResistorImpl <em>Vertical Variabel Resistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_Variabel_ResistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Variabel_Resistor()
		 * @generated
		 */
		EClass VERTICAL_VARIABEL_RESISTOR = eINSTANCE.getVertical_Variabel_Resistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_Variabel_ResistorImpl <em>Horizontal Variabel Resistor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_Variabel_ResistorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Variabel_Resistor()
		 * @generated
		 */
		EClass HORIZONTAL_VARIABEL_RESISTOR = eINSTANCE.getHorizontal_Variabel_Resistor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_InductorImpl <em>Vertical Inductor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_InductorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Inductor()
		 * @generated
		 */
		EClass VERTICAL_INDUCTOR = eINSTANCE.getVertical_Inductor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_InductorImpl <em>Horizontal Inductor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_InductorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Inductor()
		 * @generated
		 */
		EClass HORIZONTAL_INDUCTOR = eINSTANCE.getHorizontal_Inductor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_CapacitorImpl <em>Vertical Capacitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_CapacitorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Capacitor()
		 * @generated
		 */
		EClass VERTICAL_CAPACITOR = eINSTANCE.getVertical_Capacitor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_CapacitorImpl <em>Horizontal Capacitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_CapacitorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Capacitor()
		 * @generated
		 */
		EClass HORIZONTAL_CAPACITOR = eINSTANCE.getHorizontal_Capacitor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_AmplifierImpl <em>Vertical Amplifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_AmplifierImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Amplifier()
		 * @generated
		 */
		EClass VERTICAL_AMPLIFIER = eINSTANCE.getVertical_Amplifier();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_AmplifierImpl <em>Horizontal Amplifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_AmplifierImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Amplifier()
		 * @generated
		 */
		EClass HORIZONTAL_AMPLIFIER = eINSTANCE.getHorizontal_Amplifier();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.MotorImpl <em>Motor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.MotorImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getMotor()
		 * @generated
		 */
		EClass MOTOR = eINSTANCE.getMotor();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.GroundImpl <em>Ground</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.GroundImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getGround()
		 * @generated
		 */
		EClass GROUND = eINSTANCE.getGround();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Horizontal_LoudspeakerImpl <em>Horizontal Loudspeaker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Horizontal_LoudspeakerImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getHorizontal_Loudspeaker()
		 * @generated
		 */
		EClass HORIZONTAL_LOUDSPEAKER = eINSTANCE.getHorizontal_Loudspeaker();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Vertical_LoudspeakerImpl <em>Vertical Loudspeaker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Vertical_LoudspeakerImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getVertical_Loudspeaker()
		 * @generated
		 */
		EClass VERTICAL_LOUDSPEAKER = eINSTANCE.getVertical_Loudspeaker();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.Cabel_CornerImpl <em>Cabel Corner</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.Cabel_CornerImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getCabel_Corner()
		 * @generated
		 */
		EClass CABEL_CORNER = eINSTANCE.getCabel_Corner();

		/**
		 * The meta object literal for the '{@link Electric_Plan_DSL.impl.WireImpl <em>Wire</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Electric_Plan_DSL.impl.WireImpl
		 * @see Electric_Plan_DSL.impl.Electric_Plan_DSLPackageImpl#getWire()
		 * @generated
		 */
		EClass WIRE = eINSTANCE.getWire();

		/**
		 * The meta object literal for the '<em><b>From Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WIRE__FROM_COMPONENT = eINSTANCE.getWire_FromComponent();

		/**
		 * The meta object literal for the '<em><b>To Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WIRE__TO_COMPONENT = eINSTANCE.getWire_ToComponent();

	}

} //Electric_Plan_DSLPackage
