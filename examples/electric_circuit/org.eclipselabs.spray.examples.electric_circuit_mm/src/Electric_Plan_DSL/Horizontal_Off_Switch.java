/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Off Switch</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getHorizontal_Off_Switch()
 * @model
 * @generated
 */
public interface Horizontal_Off_Switch extends Component {
} // Horizontal_Off_Switch
