/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Capacitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Capacitor()
 * @model
 * @generated
 */
public interface Vertical_Capacitor extends Component {
} // Vertical_Capacitor
