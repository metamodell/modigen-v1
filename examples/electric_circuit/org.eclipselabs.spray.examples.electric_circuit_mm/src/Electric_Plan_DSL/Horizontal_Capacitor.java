/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Capacitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getHorizontal_Capacitor()
 * @model
 * @generated
 */
public interface Horizontal_Capacitor extends Component {
} // Horizontal_Capacitor
