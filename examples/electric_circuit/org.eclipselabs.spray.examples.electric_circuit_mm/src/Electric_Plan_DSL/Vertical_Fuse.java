/**
 */
package Electric_Plan_DSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Fuse</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Electric_Plan_DSL.Electric_Plan_DSLPackage#getVertical_Fuse()
 * @model
 * @generated
 */
public interface Vertical_Fuse extends Component {
} // Vertical_Fuse
